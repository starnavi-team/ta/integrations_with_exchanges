#!/usr/bin/env python
from setuptools import find_packages
from setuptools import setup

setup(
    name='integrations_with_exchanges',
    version='0.1.0',
    description='A Python library to interact with different exchanges api using `ccxt`. ',
    author='Andrey Kazantsev',
    author_email='heckad@yandex.ru',
    python_requires='>=3.8',
    url='https://gitlab.com/starnavi-team/ta/bitmex-integration',
    install_requires=[
        'aionursery',
        'blinker',
        'ccxt>=1.27.61',
        'websockets',
        'marshmallow-enum',
        'marshmallow-dataclass>=6.1.0',
        'more_itertools',
        'asyncio-rlock',
        'sentry-sdk',
    ],
    tests_require=[
        'pytest',
        'pytest-asyncio',
        'pytest-coverage',
        'dictdiffer',
    ],
    packages=find_packages(exclude=('tests', 'tests.*')),
)
