# Python async datamapper beside integration with different orders

A Python library to interact with different exchanges api using `ccxt`.
It is implement Rest and websocket clients.

## Installation
```
python setup.py install
```

## Quickstart

1. Create session with base order crud strategy
```python
from integrations_with_exchanges import Session
from integrations_with_exchanges.strategies.order_crud import OrderCrudStrategy

session = Session(OrderCrudStrategy())
```

2. Create order
```python
from decimal import Decimal

from integrations_with_exchanges.models.orders import OrderSide
from integrations_with_exchanges.models.orders import OrderPoint
from integrations_with_exchanges.models.orders import OrderType
from integrations_with_exchanges.models.orders import SmartOrder

order = SmartOrder(
    symbol='BTC/USD',
    side=OrderSide.buy,
    exchange_name='bitmex',
    user_credentials={...},
    enter_orders=[OrderPoint(amount=Decimal('1'), price=Decimal("8000"), type=OrderType.limit)],
    stop_loss_orders=[OrderPoint(amount=Decimal('1'), price=Decimal("7000"), type=OrderType.limit)],
    take_profit_orders=[OrderPoint(amount=Decimal('1'), price=Decimal("7000"), type=OrderType.limit)],
)
```

3. Add order to session
```python
await session.add(order)
```

4. Change order status on open
```python
order.status = OrderStatus.open
```

5. Make commit
```python
session.commit()
```

For cancel order on exchange need change status on canceled and make commit
```python
order.status = OrderStatus.canceled
session.commit()
```

Look at examples for more details
