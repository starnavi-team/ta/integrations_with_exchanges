"""
    rearrangement errors messages which we got from ccxt
"""
import json
from typing import Dict
from typing import List
from typing import Optional
from typing import TypedDict
from typing import Union

import ccxt
from ccxt.base.errors import BaseError


class ErrorDescription(TypedDict):
    type: str
    exchange: Optional[str]
    message: str


def format_ccxt_errors(exception: BaseError) -> ErrorDescription:
    """
        This function returns rearranged BaseError exception type message because sometimes ccxt library's exception
        message looks like: binanceus {"error":{"message":"Not Found","name":"HTTPError"}}. In this case we cannot
        treat it as suitable without additional manipulation. If exception have different from BaseError exception type
        function raises ValueError exception.

        After rearrangement result will look like:
        {
            "type": "some_type",
            "exchange": "exchange name",
            "message": "Some string"
        }
        or if exception has a dict with unknown structure without 'error' key:
        {
            "type": "some_type",
            "exchange": "exchange name",
            "message": '{"key1": "value1","key2": "value2"}'
        }
        or if exception body didn't start with a prefix:
        {
            "type": "some_type",
            "message": "Some string or raw exception body"
        }
    """
    if not isinstance(exception, BaseError):
        raise TypeError('Invalid exception type passed')

    exception_body: str = str(exception).strip()

    # We don't know exchange
    if not exception_body.startswith(tuple(ccxt.exchanges)):
        # we understood that exception_body do not start with a prefix. So exception_body is a simple string or
        # right formed JSON object and we do not exactly know its type and structure
        return {
            'type': exception.__class__.__name__,
            'exchange': None,
            'message': exception_body,
        }

    prefix, body_without_prefix = map(str.strip, exception_body.split(' ', 1))

    def parse_body(text: str) -> str:
        try:
            errors = json.loads(text)
        except json.JSONDecodeError:
            return text

        if isinstance(errors, dict):
            # we can try to parse errors
            error_key_value: Optional[Union[Dict, str, List]] = errors.get('error')

            if error_key_value is None:
                return text
            else:
                if isinstance(error_key_value, dict):
                    return error_key_value.get('message', json.dumps(error_key_value))
                elif isinstance(error_key_value, list):
                    return json.dumps(error_key_value)
                else:
                    return error_key_value
        else:
            return text

    return {
        'type': exception.__class__.__name__,
        'exchange': prefix,
        'message': parse_body(body_without_prefix)
    }
