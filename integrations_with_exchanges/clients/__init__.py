import inspect
from typing import Dict
from typing import Type

from .rest import BaseRestClient
from .websocket import BaseWebsocketClient


def get_all_subclasses(cls):
    all_subclasses = []

    for subclass in cls.__subclasses__():
        if inspect.isabstract(subclass):
            all_subclasses.extend(get_all_subclasses(subclass))
        else:
            all_subclasses.append(subclass)

    return set(all_subclasses)

REST_CLIENTS: Dict[str, Type[BaseRestClient]] = {
    clazz.name: clazz for clazz in get_all_subclasses(BaseRestClient)  # type: ignore
}

WEBSOCKET_CLIENTS: Dict[str, Type[BaseWebsocketClient]] = {
    clazz.name: clazz for clazz in get_all_subclasses(BaseWebsocketClient)  # type: ignore
}
