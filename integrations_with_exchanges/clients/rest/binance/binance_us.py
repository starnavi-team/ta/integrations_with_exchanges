from ccxt.async_support import binanceus as BinanceUs

from integrations_with_exchanges.clients.rest.binance.base import BaseBinance


class BinanceUsRestClient(BaseBinance):
    name = 'binanceus'
    ccxt_exchange_class = BinanceUs
    _client: BinanceUs
