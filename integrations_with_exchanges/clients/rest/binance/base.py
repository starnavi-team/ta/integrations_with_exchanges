from abc import ABC
from typing import Any
from typing import Dict
from typing import Tuple
from typing import Type
from typing import TypeVar

from ccxt import OrderNotFound

from integrations_with_exchanges.clients.rest import BaseRestClient
from integrations_with_exchanges.models.margin import MarginType
from integrations_with_exchanges.models.orders import OrderPoint
from integrations_with_exchanges.models.orders import OrderPointGroupType
from integrations_with_exchanges.models.orders import OrderStatus
from integrations_with_exchanges.models.orders import OrderType
from integrations_with_exchanges.types import MarginLevel

T = TypeVar('T', bound='BaseBinance')


class BaseBinance(BaseRestClient, ABC):
    ORDER_TYPE_MAPPER = {
        (OrderPointGroupType.enter, 'market'): 'MARKET',
        (OrderPointGroupType.enter, 'limit'): 'LIMIT',
        (OrderPointGroupType.stop_loss, 'market'): 'STOP_MARKET',
        (OrderPointGroupType.stop_loss, 'limit'): 'STOP',
        (OrderPointGroupType.take_profit, 'market'): 'TAKE_PROFIT_MARKET',
        (OrderPointGroupType.take_profit, 'limit'): 'TAKE_PROFIT',
    }

    @classmethod
    async def create(
        cls: Type[T],
        api_key=None,
        api_secret=None,
        password=None,
        is_test=False,
        **kwargs
    ) -> T:
        return await cls._create({
            'apiKey': api_key,
            'secret': api_secret,
            'password': password,
            'is_test': is_test
        })

    def additional_params(
        self,
        order_point: OrderPoint,
    ) -> Dict[str, Any]:
        params = super().additional_params(order_point)
        if order_point.group_type == OrderPointGroupType.enter:
            params['execInst'] = 'ParticipateDoNotInitiate'
        if order_point.type == OrderType.limit:
            params['timeInForce'] = 'GTC'
        if order_point.group_type != OrderPointGroupType.enter:
            params['stopPrice'] = order_point.price
        return params

    def _extract_order_status(self, e: OrderNotFound) -> OrderStatus:
        if 'Order already done' in str(e):
            return OrderStatus.closed
        else:
            raise ValueError("Can't parse order status")

    async def get_margin(self, symbol: str) -> Tuple[MarginType, MarginLevel]:
        raise NotImplementedError

    async def set_margin(self, symbol: str, level: MarginLevel = 0, type: MarginType = MarginType.cross) -> None:
        raise NotImplementedError

    async def _open_order_point(self, order_point: OrderPoint):
        assert order_point.order is not None
        assert order_point.side is not None
        assert order_point.group_type is not None

        return await self._client.create_order(
            str(order_point.symbol),
            str(self.ORDER_TYPE_MAPPER[(order_point.group_type, order_point.type.name)]),
            str(order_point.side.value),
            order_point.amount,
            order_point.price if order_point.type == OrderType.limit else None,
            params=self.additional_params(order_point)
        )

    async def _update_order_point(self, order_point: OrderPoint):
        assert order_point.order is not None
        assert order_point.side is not None
        assert order_point.group_type is not None

        await self._client.cancel_order(order_point.additional_info['id'], symbol=order_point.symbol)
        return await self._client.create_order(
            str(order_point.symbol),
            str(self.ORDER_TYPE_MAPPER[(order_point.group_type, order_point.type.name)]),
            str(order_point.side.value),
            str(order_point.amount),
            str(order_point.price),
            params=self.additional_params(order_point)
        )
