from ccxt.async_support import binance as Binance

from integrations_with_exchanges.clients.rest.binance.base import BaseBinance


class BinanceRestClient(BaseBinance):
    name = 'binance'
    ccxt_exchange_class = Binance
    _client: Binance
