from integrations_with_exchanges.clients.ccxt_wrappers import BinanceFuture
from integrations_with_exchanges.clients.rest.binance.base import BaseBinance


class BinanceFutureRestClient(BaseBinance):
    name = 'binancefuture'
    ccxt_exchange_class = BinanceFuture
    _client: BinanceFuture
