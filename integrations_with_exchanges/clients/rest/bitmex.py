import re
from decimal import Decimal
from typing import Any
from typing import Dict
from typing import Tuple

from ccxt.base.errors import OrderNotFound

from integrations_with_exchanges.clients.ccxt_wrappers import Bitmex
from integrations_with_exchanges.clients.rest.base import BaseRestClient
from integrations_with_exchanges.models.margin import MarginType
from integrations_with_exchanges.models.orders import OrderPoint
from integrations_with_exchanges.models.orders import OrderPointGroupType
from integrations_with_exchanges.models.orders import OrderStatus
from integrations_with_exchanges.models.orders import OrderType
from integrations_with_exchanges.types import MarginLevel


class BitmexRestClient(BaseRestClient):
    name = 'bitmex'
    ccxt_exchange_class = Bitmex
    _client: Bitmex

    ORDER_TYPE_MAPPER = {
        (OrderPointGroupType.enter, 'market'): 'Market',
        (OrderPointGroupType.enter, 'limit'): 'Limit',
        (OrderPointGroupType.stop_loss, 'market'): 'Stop',
        (OrderPointGroupType.stop_loss, 'limit'): 'StopLimit',
        (OrderPointGroupType.take_profit, 'market'): 'MarketIfTouched',
        (OrderPointGroupType.take_profit, 'limit'): 'LimitIfTouched',
    }

    @classmethod
    async def create(cls, api_key=None, api_secret=None, is_test=False, **kwargs) -> 'BitmexRestClient':
        return await cls._create({'apiKey': api_key, 'secret': api_secret, 'is_test': is_test, **kwargs})

    def additional_params(
        self,
        order_point: OrderPoint,
    ) -> Dict[str, Any]:
        params = super().additional_params(order_point)
        if order_point.group_type != OrderPointGroupType.enter:
            params['stopPx'] = str(order_point.price)
            params['execInst'] = 'Close,LastPrice'

        if order_point.is_post_only:
            params['execInst'] = 'ParticipateDoNotInitiate'

        if order_point.order is not None:
            params['text'] = str(order_point.order.id)

        return params

    def _extract_order_status(self, e: OrderNotFound) -> OrderStatus:
        state_regexp = re.compile(r"state: (\w+)")

        matched = state_regexp.search(str(e))

        if matched is None:
            raise ValueError("Can't extract status")

        status = matched.group(1)
        status = self._client.parse_order_status(status)
        return OrderStatus(status)

    async def get_margin(self, symbol: str) -> Tuple[MarginType, MarginLevel]:
        await self._client.load_markets()

        position = await self._client.privateGetPosition({
            'symbol': self._client.market_id(symbol.upper())
        })
        position = position[0]

        return MarginType.cross if position['crossMargin'] else MarginType.isolated, position['leverage']

    async def set_margin(self, symbol: str, level: MarginLevel = 0, type: MarginType = MarginType.cross) -> None:
        await self._client.load_markets()
        await self._client.privatePostPositionLeverage({
            'symbol': self._client.market_id(symbol.upper()),
            'leverage': level
        })

    async def _open_order_point(self, order_point: OrderPoint):
        assert order_point.order is not None
        assert order_point.side is not None
        assert order_point.group_type is not None

        return await self._client.create_order(
            str(order_point.symbol),
            str(self.ORDER_TYPE_MAPPER[(order_point.group_type, order_point.type.value)]),
            str(order_point.side.value),
            str(order_point.amount),
            str(order_point.price) if order_point.type == OrderType.limit else None,
            params=self.additional_params(order_point)
        )

    async def _update_order_point(self, order_point: OrderPoint):
        assert order_point.order is not None
        assert order_point.side is not None
        assert order_point.group_type is not None

        return await self._client.edit_order(
            id=str(order_point.additional_info['id']),
            symbol=None,
            type=None,
            side=None,
            amount=str(order_point.amount),
            price=str(order_point.price) if order_point.type == OrderType.limit else None,
            params=self.additional_params(order_point)
        )

    async def round_price(self, price: Decimal, symbol: str) -> Decimal:
        symbol_precision = await self.get_symbol_precision(symbol)
        tik = Decimal(symbol_precision['price']).quantize(Decimal('1.000000000000000'))
        return price // tik * tik
