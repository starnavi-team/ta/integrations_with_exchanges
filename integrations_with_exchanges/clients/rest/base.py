from abc import ABC
from abc import abstractmethod
from abc import abstractproperty
from datetime import datetime
from decimal import Decimal
from typing import Any
from typing import ClassVar
from typing import Dict
from typing import Generic
from typing import Optional
from typing import Tuple
from typing import Type
from typing import TypeVar

import asyncio_rlock
from ccxt.async_support import Exchange
from ccxt.base import errors as ccxt_errors
from ccxt.base.errors import OrderNotFound
from loguru import logger

from integrations_with_exchanges.exc import NotSupportedOrderSymbol
from integrations_with_exchanges.models.margin import MarginType
from integrations_with_exchanges.models.orders import OrderPoint
from integrations_with_exchanges.models.orders import OrderStatus
from integrations_with_exchanges.models.orders import SmartOrder
from integrations_with_exchanges.types import MarginLevel

A = TypeVar('A', bound='BaseRestClient')


class BaseRestClient(ABC, Generic[A]):
    name: ClassVar[str] = abstractproperty(None)  # type: ignore
    ccxt_exchange_class: ClassVar[Type[Exchange]] = abstractproperty(None)  # type: ignore

    _CREATE_LOCK: ClassVar[asyncio_rlock.RLock]

    @classmethod
    async def _init_base(cls: Type[A], is_test: bool = False) -> A:
        if not hasattr(cls, '_CREATE_LOCK'):
            cls._CREATE_LOCK = asyncio_rlock.RLock()

        if not hasattr(cls, '_BaseRestClient__client'):
            async with cls._CREATE_LOCK:
                if not hasattr(cls, '_BaseRestClient__client'):
                    client = cls.ccxt_exchange_class()
                    client.enableRateLimit = False
                    client.set_sandbox_mode(is_test)
                    # call for caching
                    await client.load_markets()

                    setattr(cls, '_BaseRestClient__client', client)

        return getattr(cls, '_BaseRestClient__client')

    @classmethod
    async def _create(cls: Type[A], config=None) -> A:
        if config is None:
            config = {}

        is_test = config.get('is_test', False)

        base_client = await cls._init_base(is_test)

        new_client = cls.ccxt_exchange_class(config)
        new_client.set_sandbox_mode(is_test)
        new_client.markets = base_client.markets
        new_client.symbols = base_client.symbols
        new_client.currencies = base_client.currencies
        new_client.ids = base_client.ids

        new_client.markets_by_id = new_client.marketsById = base_client.markets_by_id

        return cls(new_client)

    def __init__(self, clinet: Exchange):
        self._client = clinet

        if __debug__:
            self.debug_logger = logger

    @classmethod
    @abstractmethod
    async def create(cls: Type[A], **kwargs) -> A:
        raise NotImplementedError

    @abstractmethod
    def _extract_order_status(self, e: OrderNotFound) -> OrderStatus:
        raise NotImplementedError

    def additional_params(
        self,
        order_point: OrderPoint,
    ) -> Dict[str, Any]:
        return {}

    async def validate_order(self, order: SmartOrder):
        await self._client.load_markets()

        try:
            self._client.market_id(order.symbol)
        except ccxt_errors.BadSymbol:
            raise NotSupportedOrderSymbol

    async def get_balance(self):
        await self._client.load_markets()
        return await self._client.fetch_balance()

    async def get_open_orders(self, symbol=None, since=None, limit=None):
        await self._client.load_markets()
        return await self._client.fetch_open_orders(symbol, since, limit)

    async def get_symbol_precision(self, symbol):
        await self._client.load_markets()
        return self._client.markets[symbol]['precision']

    async def get_order_book(self, symbol):
        return await self._client.fetch_order_book(symbol)

    async def get_status(self, order_point: OrderPoint, order_info: Optional[Dict[str, Any]] = None) -> OrderStatus:
        if order_info is None:
            order_info = await self._client.fetch_order(order_point.additional_info['id'])
        return OrderStatus(order_info['status'])

    async def get_order_info(self, order_point: OrderPoint) -> Dict['str', Any]:
        assert order_point.symbol is not None

        return await self._client.fetch_order(order_point.additional_info['id'], symbol=order_point.symbol)

    @abstractmethod
    async def get_margin(self, symbol: str) -> Tuple[MarginType, MarginLevel]:
        pass

    @abstractmethod
    async def set_margin(self, symbol: str, level: MarginLevel = 0, type: MarginType = MarginType.cross) -> None:
        pass

    async def synchronize_order_status(self, order: SmartOrder):
        for o in order.points:
            if o.is_placed_on_exchange:
                o.status = OrderStatus((await self._client.fetch_order(o.additional_info['id']))['status'])

    async def round_amount(self, amount: Decimal, symbol: str) -> Decimal:
        symbol_precision = await self.get_symbol_precision(symbol)
        return amount.quantize(1 / Decimal(10 ** symbol_precision['amount']))

    async def round_price(self, price: Decimal, symbol: str) -> Decimal:
        symbol_precision = await self.get_symbol_precision(symbol)
        return price.quantize(1 / Decimal(10 ** symbol_precision['price']))

    async def open_order(self, order: SmartOrder) -> None:
        # Open each point separately
        # If exchange support bulk open need can be override in children

        order_points = order.points if order.place_now else order.enter_orders

        for op in order_points:
            if op.status == OrderStatus.pending and not op.is_condition:
                await self.open_order_point(op)

                if op.status.is_failed:
                    order.status = op.status  # if at least one is not open then the whole order is
                    order.additional_info.setdefault('reason', op.additional_info['reason'])
                    await self.cancel_opened_order_points(order)

    async def open_order_point(self, point: OrderPoint) -> OrderPoint:
        # assert 'status' in point.changes and \
        #        point.changes['status'] == {
        #            'op': 'update',
        #            'new': OrderStatus.open,
        #            'old': OrderStatus.pending
        #        }
        assert not point.is_placed_on_exchange, "Only not placing order points can be placed on an exchange"

        assert point.symbol is not None

        if __debug__:
            self.debug_logger.debug(f"Open order point with id: {point.id}")

        point.amount = await self.round_amount(point.amount, point.symbol)
        point.price = await self.round_price(point.price, point.symbol)

        resp = await self._open_order_point(point)

        if __debug__:
            self.debug_logger.debug(f"The order point with id: {point.id} opened on exchange")

        point.status = OrderStatus(resp['status'])
        point.additional_info['id'] = resp['id']
        point.additional_info['datetime'] = resp['datetime'] or str(datetime.utcnow())

        return point

    @abstractmethod
    async def _open_order_point(
        self,
        order_point: OrderPoint
    ) -> Dict[str, Any]:
        pass

    async def update_order_point(self, point: OrderPoint) -> OrderPoint:
        assert point.status == OrderStatus.open, "The order point should be open on an exchange"
        assert point.is_placed_on_exchange, "The order point should be placed on an exchange"

        assert point.symbol is not None

        if __debug__:
            self.debug_logger.debug(f"Update order point with id: {point.id}")

        point.amount = await self.round_amount(point.amount, point.symbol)
        point.price = await self.round_price(point.price, point.symbol)

        resp = await self._update_order_point(point)

        if __debug__:
            self.debug_logger.debug(f"The order point with id: {point.id} updated on exchange")

        point.status = OrderStatus(resp['status'])
        point.additional_info['id'] = resp['id']
        point.additional_info['datetime'] = resp['datetime'] or str(datetime.utcnow())

        return point

    @abstractmethod
    async def _update_order_point(
        self,
        order_point: OrderPoint,
    ) -> Dict[str, Any]:
        pass

    async def cancel_order_point(self, point: OrderPoint) -> OrderPoint:
        assert 'id' in point.additional_info

        await self._cancel_order_point(point)

        return point

    async def _cancel_order_point(self, point: OrderPoint) -> None:
        try:
            await self._client.cancel_order(point.additional_info['id'], symbol=point.symbol)
        except OrderNotFound:
            pass
        else:
            point.status = OrderStatus.canceled

    async def close_order(self, order: SmartOrder):
        await self.cancel_opened_order_points(order)

        for op in order.points:
            if op.status == OrderStatus.pending:
                op.status = OrderStatus.closed

        order.status = OrderStatus.closed
        order.closed_at = datetime.now()

    async def cancel_order(self, order: SmartOrder):
        await self.cancel_opened_order_points(order)

        for op in order.points:
            if op.status == OrderStatus.pending:
                op.status = OrderStatus.canceled

        order.status = OrderStatus.canceled

    async def cancel_opened_order_points(self, order):
        for o in order.points:
            if o.status == OrderStatus.open:
                if o.is_placed_on_exchange:
                    await self.cancel_order_point(o)
                else:
                    o.status = OrderStatus.canceled

    async def __aenter__(self):
        await self._client.__aenter__()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        return await self._client.__aexit__(exc_type, exc_val, exc_tb)

    async def close(self):
        return await self._client.close()
