from typing import Any
from typing import Dict
from typing import Tuple

from ccxt.async_support import coinbasepro as CoinbasePro
from ccxt.base.errors import OrderNotFound

from integrations_with_exchanges.clients.rest.base import BaseRestClient
from integrations_with_exchanges.models.margin import MarginType
from integrations_with_exchanges.models.orders import OrderPoint
from integrations_with_exchanges.models.orders import OrderPointGroupType
from integrations_with_exchanges.models.orders import OrderSide
from integrations_with_exchanges.models.orders import OrderStatus
from integrations_with_exchanges.types import MarginLevel


class CoinbaseProRestClient(BaseRestClient):
    name = 'coinbasepro'
    ccxt_exchange_class = CoinbasePro
    _client: CoinbasePro

    @classmethod
    async def create(
        cls,
        api_key=None,
        api_secret=None,
        password=None,
        is_test=False,
        **kwargs
    ) -> 'CoinbaseProRestClient':
        return await cls._create({
            'apiKey': api_key,
            'secret': api_secret,
            'password': password,
            'is_test': is_test
        })

    def additional_params(
        self,
        order_point: OrderPoint,
    ) -> Dict[str, Any]:
        params = super().additional_params(order_point)
        if order_point.group_type != OrderPointGroupType.enter:
            if order_point.group_type == OrderPointGroupType.stop_loss:
                if order_point.side == OrderSide.sell:
                    params['stop'] = "loss"
                else:
                    params['stop'] = "entry"
                params['stop_price'] = str(order_point.price)
        params['post_only'] = order_point.is_post_only
        return params

    def _extract_order_status(self, e: OrderNotFound) -> OrderStatus:
        if 'Order already done' in str(e):
            return OrderStatus.closed
        else:
            raise ValueError("Can't parse order status")

    async def get_margin(self, symbol: str) -> Tuple[MarginType, MarginLevel]:
        raise NotImplementedError

    async def set_margin(self, symbol: str, level: MarginLevel = 0, type: MarginType = MarginType.cross) -> None:
        raise NotImplementedError

    async def _open_order_point(self, order_point: OrderPoint):
        assert order_point.order is not None
        assert order_point.side is not None
        assert order_point.group_type is not None

        return await self._client.create_order(
            str(order_point.symbol),
            str(order_point.type.value),
            str(order_point.side.value),
            str(order_point.amount),
            str(order_point.price),
            params=self.additional_params(order_point)
        )

    async def _update_order_point(self, order_point: OrderPoint):
        assert order_point.order is not None
        assert order_point.side is not None
        assert order_point.group_type is not None

        await self._client.cancel_order(order_point.additional_info['id'])
        return await self._client.create_order(
            str(order_point.symbol),
            str(order_point.type.value),
            str(order_point.side.value),
            str(order_point.amount),
            str(order_point.price),
            params=self.additional_params(order_point)
        )
