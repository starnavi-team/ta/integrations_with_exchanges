import json
from abc import ABCMeta
from typing import Any
from typing import Dict

from integrations_with_exchanges.clients.websocket.base import BaseWebsocketClient
from integrations_with_exchanges.clients.rest import BinanceRestClient


class BinanceWebsocketClient(BaseWebsocketClient, metaclass=ABCMeta):
    name = 'binance'

    SUBSCRIBE_MESSAGE_STRING = json.dumps({
        "method": "SUBSCRIBE",
        "params": ['{real_symbol}@aggTrade']
    })

    def __init__(self, is_test=False):
        url = 'wss://stream.binance.com:9443/stream?streams='

        super().__init__(url, BinanceRestClient, is_test)

    async def _dispatch_message(self, message):
        if 'result' in message and message['result'] is None:  # Need because result by default None
            return await self.on_success(message)

        return await self.on_message(message)

    def _subscribe_message_factory(self, **params) -> Dict[str, Any]:
        stream_name = "depth10" if params.get('topic') == 'order_bookL2' else 'trade'

        return {
            "id": 1,
            "method": "SUBSCRIBE",
            "params": [f'{params["real_symbol"].lower()}@{stream_name}']
        }

    async def _build_login_message(self, credentials) -> dict:
        pass

    async def _subscribe_on_order_bookL2(self, connection, symbol):
        pass
