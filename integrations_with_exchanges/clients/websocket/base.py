import asyncio
import json
from abc import ABC
from abc import abstractmethod
from abc import abstractproperty
from contextlib import contextmanager
from typing import Any
from typing import Awaitable
from typing import Callable
from typing import ClassVar
from typing import Dict
from typing import Optional
from typing import Set
from typing import Type
from typing import Union

import aionursery
import websockets
from aionursery import MultiError
from asyncio_rlock import RLock
from loguru import logger
from sentry_sdk import capture_exception
from websockets import ConnectionClosedError
from websockets import WebSocketClientProtocol

from integrations_with_exchanges.utils import cache_in_class_instance
from integrations_with_exchanges.utils import Partial as partial
from integrations_with_exchanges.utils import Queue
from ..rest import BaseRestClient

WebsocketCommand = Callable[[WebSocketClientProtocol], Awaitable[Any]]

OnMessageHandlerClassMethod = Union[
    Callable[[Dict], Awaitable[None]],
    Callable[[Dict], None],
]
OnMessageHandlerFunction = Union[
    Callable[['BaseWebsocketClient', Dict], Awaitable[None]],
    Callable[['BaseWebsocketClient', Dict], None]
]

OnMessageHandler = Union[
    OnMessageHandlerClassMethod,
    OnMessageHandlerFunction
]


class BaseWebsocketClient(ABC):
    name: ClassVar[str] = abstractproperty(None)  # type: ignore

    def __init__(self,
                 websocket_url,
                 rest_client_class: Type[BaseRestClient],
                 is_test: bool = False):
        self.websocket_url = websocket_url
        self._rest_client_class = rest_client_class
        self.is_test = is_test
        self.stopped = False

        self.new_commands: Queue[WebsocketCommand] = Queue()
        self.used_commands: Set[WebsocketCommand] = set()

        self._connection_lock = RLock()

        self._map_symbol_to_market_id: Optional[Dict[str, str]] = None
        self._map_market_id_to_symbol: Optional[Dict[str, str]] = None

        self._nursery_in_run: Optional[aionursery.Nursery] = None

    @cache_in_class_instance()
    async def _get_rest_client(self, **credentials):
        return await self._rest_client_class.create(
            **credentials, is_test=self.is_test)

    @abstractmethod
    def _subscribe_message_factory(self, **params) -> Dict[str, Any]:
        pass

    async def _subscribe_on(self, connection, *, topic=None, symbol=None):
        real_symbol = await self.convert_symbol_to_market_id(symbol)

        await connection.send(
            json.dumps(self._subscribe_message_factory(
                topic=topic,
                real_symbol=real_symbol
            ))
        )

    @abstractmethod
    async def _subscribe_on_order_bookL2(self, connection, symbol):
        pass

    def subscribe_on(self, *, topic: str = None, symbol: str = None):
        self.new_commands.put_nowait(
            partial(self._subscribe_on, topic=topic, symbol=symbol)
        )

    async def _handle_commands(self, connection: WebSocketClientProtocol):
        async for func in self.new_commands:
            if func in self.used_commands:
                continue

            try:
                await func(connection=connection)
            except Exception as e:
                capture_exception(e)
            else:
                self.used_commands.add(func)

    async def _listen_new_messages(self, connection: WebSocketClientProtocol):
        async for message in connection:
            with logger.catch(reraise=False):
                await asyncio.create_task(
                    self._dispatch_message(
                        json.loads(message)
                    )
                )

    @abstractmethod
    async def _dispatch_message(self, message):
        raise NotImplementedError

    @abstractmethod
    async def on_message(self, message: Dict):
        raise NotImplementedError

    async def on_error(self, message: Dict):
        logger.error(message)

    async def on_success(self, message: Dict):
        logger.info(message)

    async def _init_converters(self):
        async with await self._rest_client_class.create(is_test=self.is_test) as rest_client:
            markets = rest_client._client.markets

            self._map_symbol_to_market_id = \
                {k: v['id'] for k, v in markets.items()}
            self._map_market_id_to_symbol = \
                {v['id']: k for k, v in markets.items()}

    async def convert_symbol_to_market_id(self, symbol: str) -> str:
        if self._map_symbol_to_market_id is None:
            await self._init_converters()

        assert self._map_symbol_to_market_id is not None

        return self._map_symbol_to_market_id[symbol.upper()]

    async def convert_market_id_to_symbol(self, market_id: str) -> str:
        if self._map_market_id_to_symbol is None:
            await self._init_converters()

        assert self._map_market_id_to_symbol is not None

        return self._map_market_id_to_symbol[market_id.upper()]

    async def _run(self):
        async with websockets.connect(self.websocket_url, ) as connection:
            async with aionursery.Nursery() as nursery:
                self._nursery_in_run = nursery  # Save nursery for cancel in future

                nursery.start_soon(self._handle_commands(connection))
                nursery.start_soon(self._listen_new_messages(connection))

    async def run(self):
        while not self.stopped:
            with self._handle_error():
                await self._run()

    @contextmanager
    def _handle_error(self):
        try:
            yield
        except MultiError as error:
            if all(isinstance(e, ConnectionClosedError) for e in error.exceptions):
                for item in self.used_commands:
                    self.new_commands.put_nowait(item)

                self.used_commands = set()
                return True

        return False  # Default

    def stop(self):
        if not self.stopped:
            if self._nursery_in_run is not None:
                self._nursery_in_run.cancel_remaining()
            self.stopped = True
