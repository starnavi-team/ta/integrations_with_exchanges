import hashlib
import hmac
import json
import urllib
import urllib.parse
from abc import ABCMeta
from datetime import datetime
from datetime import timedelta
from datetime import timezone
from typing import Dict

from loguru import logger

from integrations_with_exchanges.clients.rest.bitmex import BitmexRestClient
from integrations_with_exchanges.clients.websocket.base import BaseWebsocketClient


class BitmexWebsocketClient(BaseWebsocketClient, metaclass=ABCMeta):
    name = 'bitmex'

    SUBSCRIBE_MESSAGE_STRING = json.dumps({
        'op': 'subscribe',
        'args': ['trade:{real_symbol}']
    })

    def __init__(self, is_test=False):
        if is_test:
            url = "wss://testnet.bitmex.com/realtime"
        else:
            url = "wss://www.bitmex.com/realtime"

        super().__init__(url, BitmexRestClient, is_test)

    async def _dispatch_message(self, message):
        if 'success' in message:
            return await self.on_success(message)
        if 'error' in message:
            return await self.on_error(message)

        return await self.on_message(message)

    def _subscribe_message_factory(self, **params):
        if params['topic'] == 'order_bookL2':
            topic = 'orderBookL2_25'
        else:
            topic = params['topic']

        return {
            'op': 'subscribe',
            'args': [f'{topic}:{params["real_symbol"]}']
        }

    async def _build_login_message(self, credentials: Dict[str, str]):
        expires_date = datetime.now() + timedelta(seconds=5)
        expires = int(expires_date.replace(tzinfo=timezone.utc).timestamp())
        return {
            "op": 'authKeyExpires',
            "args": [credentials['api_key'], expires, bitmex_signature(
                credentials['api_secret'], 'GET', '/realtime', expires,
            )]
        }

    async def _subscribe_on_order_bookL2(self, connection, symbol):
        real_symbol = await self.convert_symbol_to_market_id(symbol)

        await connection.send(
            json.dumps({'op': 'subscribe', 'args': [f'orderBookL2_25:{real_symbol}']})
        )

        # self._subscribed_on['symbols'].add(symbol)


def bitmex_signature(api_secret, verb, url, nonce, post_dict=None):
    """Given an API Secret key and data, create a BitMEX-compatible signature."""
    data = ''
    if post_dict:
        # separators remove spaces from json
        # BitMEX expects signatures from JSON built without spaces
        data = json.dumps(post_dict, separators=(',', ':'))
    parsedURL = urllib.parse.urlparse(url)
    path = parsedURL.path
    if parsedURL.query:
        path = path + '?' + parsedURL.query
    # print("Computing HMAC: %s" % verb + path + str(nonce) + data)
    message = (verb + path + str(nonce) + data).encode('utf-8')
    logger.debug('Signing: %s' % str(message))

    signature = hmac.new(
        api_secret.encode('utf-8'), message,
        digestmod=hashlib.sha256,
    ).hexdigest()
    logger.debug('Signature: %s' % signature)
    return signature
