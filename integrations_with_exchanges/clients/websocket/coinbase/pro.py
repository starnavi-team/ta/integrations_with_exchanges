from abc import ABCMeta
from typing import Dict
from typing import Any

from integrations_with_exchanges.clients.rest import CoinbaseProRestClient
from integrations_with_exchanges.clients.websocket.base import BaseWebsocketClient


class CoinbaseProWebsocketClient(BaseWebsocketClient, metaclass=ABCMeta):
    name = 'coinbasepro'

    def __init__(self, is_test=False):
        if is_test:
            url = "wss://ws-feed-public.sandbox.pro.coinbase.com"
        else:
            url = "wss://ws-feed.pro.coinbase.com"

        super().__init__(url, CoinbaseProRestClient, is_test)

    async def _dispatch_message(self, message):
        if message['type'] == 'subscriptions':
            return await self.on_success(message)
        if message['type'] == 'error':
            return await self.on_error(message)

        return await self.on_message(message)

    def _subscribe_message_factory(self, **params) -> Dict[str, Any]:
        if params['topic'] == 'order_bookL2':
            topic = 'level2'
        elif params['topic'] == 'trade':
            topic = 'ticker'
        else:
            topic = params['topic']

        return {
            "type": "subscribe",
            "channels": [{
                "name": topic,
                "product_ids": [f'{params["real_symbol"]}']
            }]
        }

    async def _build_login_message(self, credentials) -> dict:
        pass

    async def _subscribe_on_order_bookL2(self, connection, symbol):
        pass
