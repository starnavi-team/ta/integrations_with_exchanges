import asyncio

import ccxt
from ccxt.async_support import binance as BaseBinance
from ccxt.async_support import bitmex as BaseBitmex
from ccxt.async_support import Exchange
from ccxt.base.errors import ExchangeError
from ccxt.base.errors import OrderNotFound

from integrations_with_exchanges.utils import retry

Exchange.fetch2 = retry(
    max_retries_count=12,
    on_exceptions=(
        ccxt.errors.NetworkError,
        asyncio.TimeoutError
    )
)(Exchange.fetch2)


class Bitmex(BaseBitmex):
    def handle_errors(self, code, reason, url, method, headers, body, response, requestHeaders, requestBody):
        try:
            super().handle_errors(code, reason, url, method, headers, body, response, requestHeaders, requestBody)
        except ExchangeError as e:
            message = str(e)
            if message == 'bitmex {"error":{"message":"Not Found","name":"HTTPError"}}':
                raise OrderNotFound(message)
            raise


class BinanceFuture(BaseBinance):
    def describe(self):
        res = super().describe()
        res['name'] = 'BinanceFuture'

        res['options']['defaultType'] = 'future'

        return res
