from itertools import chain
from typing import Dict
from typing import Iterable
from typing import List
from typing import Optional
from typing import TypeVar

from integrations_with_exchanges import exc
from integrations_with_exchanges.models.base import Model
from integrations_with_exchanges.models.state import InstanceState
from integrations_with_exchanges.session.config import SessionConfig
from integrations_with_exchanges.session.strategy import SessionStrategy
from integrations_with_exchanges.utils import atomic_method
from integrations_with_exchanges.utils import get_state

T = TypeVar('T')


class Session:
    def __init__(self, strategy: SessionStrategy, config: SessionConfig = SessionConfig()):
        self.config = config
        self._expire_objects: List[Model] = []
        self._objects: Dict[InstanceState, Model] = {}
        self._new_objects: Dict[InstanceState, Model] = {}
        self._strategy: SessionStrategy = strategy

    def __iter__(self):
        return chain(iter(self._new_objects.values()), iter(self._objects.values()))

    def __contains__(self, item):
        state = get_state(item)

        return state in self._new_objects or state in self._objects

    def add(self, instance):
        if not self._strategy.is_support_object(instance):
            raise TypeError("Not support object")

        state = get_state(instance)

        if state.key is not None:
            raise exc.InvalidRequestError(
                "Object '{}' already has an identity - "
                "it can't be registered as pending".format(state)
            )

        def add_instance(instance):
            state = get_state(instance)
            if state.key is None:
                state.key = id(instance)
                obj = state.instance()
                self._new_objects[state] = obj
                state.session = self

        def check_and_add_instance(instance):
            if isinstance(instance, Model):
                add_instance(instance)

        add_instance(instance)

        # todo: add ability for work with any type of objects

        # for v in vars(instance).values():
        #     if self._strategy.is_support_object(v):
        #         check_and_add_instance(v)
        #
        #     elif isinstance(v, Iterable) and not isinstance(v, str):
        #         for o in v:
        #             check_and_add_instance(o)

    def expunge(self, instance):
        state = get_state(instance)

        if state in self._new_objects:
            self._new_objects.pop(state)
        elif state in self._objects:
            self._objects.pop(state)
        else:
            raise ValueError("Session doesn't contain instance")

    async def get(self, key):
        obj = await self._strategy.get(key)
        state = get_state(obj)
        self._objects[state] = obj
        return obj

    @atomic_method
    async def synchronize(self, do_commit_after_sync: bool = True):
        strategy = self._strategy

        for o in self:
            await strategy.synchronize(o)
            await strategy.on_complete_sync(o)

        if do_commit_after_sync:
            await self.commit()

    async def synchronize_only(self, instance):
        await self._strategy.synchronize(instance)
        await self._strategy.on_complete_sync(instance)
        await self.commit_only(instance)

    @atomic_method
    async def commit(self):
        strategy = self._strategy

        for o in self._new_objects.values():
            if strategy.is_support_object(o):
                await strategy.handle_new_object(o)
                await strategy.on_complete_handle(o)
            else:
                if self.config.raise_on_not_supported_objects:
                    raise RuntimeError('Not supported object')

        for o in self._objects.values():
            if strategy.is_support_object(o):
                await strategy.handle_changed_object(o)
                await strategy.on_complete_handle(o)

        self._objects.update(self._new_objects)
        self._new_objects = {}

    async def commit_only(self, instance):
        if instance not in self:
            raise ValueError("Instance not contains in session")

        state = get_state(instance)

        if state in self._new_objects:
            await self._strategy.handle_new_object(instance)
            await self._strategy.on_complete_handle(instance)

            del self._new_objects[state]
            self._objects[state] = instance

        elif state in self._objects:
            await self._strategy.handle_changed_object(instance)
            await self._strategy.on_complete_handle(instance)

    def expire(self, instance, attribute_names: Optional[List[str]] = None):
        if attribute_names is not None:
            instance.expire(attribute_names)
        if not instance.is_expired:
            self._register_instance_as_expired(instance)

    def _register_instance_as_expired(self, instance):
        if hasattr(instance, 'order'):
            if instance.order is None:
                raise ValueError("Can't add free order point")
            instance = instance.order
        self._expire_objects.append(instance)
