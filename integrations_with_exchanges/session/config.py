from dataclasses import dataclass


@dataclass(frozen=True)
class SessionConfig:
    skip_not_supported_objects = True
    raise_on_not_supported_objects = False
