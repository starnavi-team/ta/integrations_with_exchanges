from abc import ABC
from abc import abstractmethod
from typing import Generic
from typing import Hashable
from typing import TypeVar

from integrations_with_exchanges.models.base import Model

T = TypeVar('T')


class SessionStrategy(ABC, Generic[T]):
    @abstractmethod
    def is_support_object(self, obj: Model) -> bool:
        raise NotImplementedError

    @abstractmethod
    async def on_complete_sync(self, obj: T) -> None:
        raise NotImplementedError

    @abstractmethod
    async def on_complete_handle(self, obj: T) -> None:
        raise NotImplementedError

    @abstractmethod
    async def get(self, key: Hashable) -> T:
        raise NotImplementedError

    @abstractmethod
    async def synchronize(self, obj: T) -> None:
        raise NotImplementedError

    @abstractmethod
    async def handle_new_object(self, obj: T) -> None:
        raise NotImplementedError

    @abstractmethod
    async def handle_changed_object(self, obj: T) -> None:
        raise NotImplementedError

    @abstractmethod
    async def handle_removed_object(self, obj: T) -> None:
        raise NotImplementedError
