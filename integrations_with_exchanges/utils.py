import asyncio
import functools
import itertools
import logging
import threading
from asyncio import Queue as _Queue
from contextlib import asynccontextmanager
from contextlib import contextmanager
from functools import partial
from itertools import chain
from itertools import count
from typing import Any
from typing import Dict
from typing import Generic
from typing import Iterable
from typing import List
from typing import Optional
from typing import Protocol
from typing import Type
from typing import TYPE_CHECKING
from typing import TypeVar
from weakref import WeakValueDictionary

import asyncio_rlock
from asyncio_rlock import RLock

from integrations_with_exchanges import attributes
from integrations_with_exchanges import exc

if TYPE_CHECKING:
    from integrations_with_exchanges import BaseRestClient
    from integrations_with_exchanges.models.state import InstanceState
    from integrations_with_exchanges.models.orders import OrderPoint
    from integrations_with_exchanges.models.orders import OrderStatus

logger = logging.getLogger(__name__)

T = TypeVar('T')


class Queue(Generic[T], _Queue):
    def __init__(self, maxsize=0, *, loop=None):
        super().__init__(maxsize, loop=loop)
        self._stopped = False

    async def __anext__(self) -> T:
        if self._stopped:
            raise StopIteration

        return await self.get()

    def __aiter__(self):
        return self

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    def put_nowait(self, item: T, raise_error: bool = True):
        if self._stopped:
            if raise_error:
                raise ValueError("Can't send item in dummy queue")
            else:
                return
        super().put_nowait(item)

    def stop(self):
        self._stopped = True

    def is_stop(self) -> bool:
        return self._stopped


def set_to_dict(to_dict, key, from_dict):
    some = from_dict.get(key)
    if some is not None:
        to_dict[key] = some


@contextmanager
def tmp_remove(_l: List, x: Any):
    _l.remove(x)
    try:
        yield
    finally:
        _l.append(x)


def is_all_have_status(ops: Iterable['OrderPoint'], status: 'OrderStatus') -> bool:
    return all(list(map(lambda o: o.status == status, ops)) or [False])


def get_state(instance) -> 'InstanceState':
    try:
        return attributes.instance_state(instance)
    except exc.NO_STATE:
        raise exc.NotSupportedObject(instance)


def counter():
    """Return a threadsafe counter function."""

    lock = threading.Lock()
    counter = itertools.count(1)

    # avoid the 2to3 "next" transformation...
    def _next():
        lock.acquire()
        try:
            return next(counter)
        finally:
            lock.release()

    return _next


class CredExchange(Protocol):
    user_credentials: Dict[str, Any]
    exchange_name: str
    is_test: bool


async def create_client(
    args: CredExchange,
    rest_clients: Optional[Dict[str, Type['BaseRestClient']]] = None
) -> 'BaseRestClient':
    if rest_clients is None:
        from integrations_with_exchanges import REST_CLIENTS
        rest_clients = REST_CLIENTS
    exchange = rest_clients.get(args.exchange_name)
    if exchange is None:
        raise ValueError("Not supported exchange")
    return await exchange.create(**args.user_credentials, is_test=args.is_test)


@asynccontextmanager
async def exchange_rest_client(args: CredExchange, rest_clients=None):
    client = await create_client(args, rest_clients)
    try:
        yield client
    finally:
        await client.close()


def retry(max_retries_count, on_exceptions: Iterable[Type[BaseException]]):
    def decorator(func):
        @functools.wraps(func)
        async def decorated(*args, **kwargs):
            for retry_num in count():
                try:
                    return await func(*args, **kwargs)
                except tuple(on_exceptions):
                    logger.exception(f"We got {retry_num} error")

                    if retry_num > max_retries_count:
                        raise

                    await asyncio.sleep(min(2 ** retry_num, 30))

        return decorated

    return decorator


def cache_in_class_instance(key=None):
    def decorator(func):
        MISSING = object()

        @functools.wraps(func)
        async def decorated(self, *args, **kwargs):
            nonlocal key

            if key is None:
                key = func.__name__ + f'{args}-{kwargs}'

            values = vars(self)

            res = values.get(key, MISSING)
            if res is not MISSING:
                return res

            async with vars(self).setdefault(f'{key}_lock', RLock()):
                res = values.get(key, MISSING)
                if res is not MISSING:
                    return res

                res = values[key] = await func(self, *args, **kwargs)
                return res

        return decorated

    return decorator


class Partial(partial):
    def __eq__(self, other):
        if not isinstance(other, Partial):
            return False

        return self.func == other.func and \
               self.args == other.args and \
               self.keywords.items() == other.keywords.items()

    def __hash__(self):
        def hash_or_id(el):
            try:
                return hash(el)
            except TypeError:
                return id(el)

        return hash(
            hash(self.func) + sum(map(hash_or_id, chain(self.args, self.keywords.items())))
        )


def atomic_method(func):
    lock_registry = WeakValueDictionary()

    @functools.wraps(func)
    async def decorated(self, *args, **kwargs):
        async with lock_registry.setdefault(id(self), asyncio_rlock.RLock()) as lock:
            return await func(self, *args, *kwargs)

    return decorated
