import typing
from typing import Union

from marshmallow import Schema as BaseSchema
from marshmallow import SchemaOpts as BaseSchemaOpts
from marshmallow import types


class SchemaOpts(BaseSchemaOpts):
    def __init__(self, meta, **kwargs):
        super().__init__(meta, **kwargs)
        self.required_fields = getattr(meta, "required_fields", set())


class Schema(BaseSchema):
    OPTIONS_CLASS = SchemaOpts

    def __init__(self, *, required_fields=set(), **kwargs):
        self.required_fields = required_fields or self.opts.required_fields
        super().__init__(**kwargs)

    def on_bind_field(self, field_name, field_obj):
        super().on_bind_field(field_name, field_obj)
        if field_name in self.required_fields:
            field_obj.required = True

    def make_data_class(self, data, **_):
        super().make_data_class(data, **_)

    def loads(
        self,
        json_data: Union[str, bytes, bytearray],
        *,
        many: bool = None,
        partial: typing.Union[bool, types.StrSequenceOrSet] = None,
        unknown: str = None,
        **kwargs
    ):
        return super().loads(
            json_data,  # type: ignore
            many=many, partial=partial, unknown=unknown, **kwargs)
