NO_STATE = (AttributeError, KeyError)


class IntegrationWithExchangesError(Exception):
    pass


class NotSupportedObject(IntegrationWithExchangesError):
    pass


class NotSupportedOrderSymbol(IntegrationWithExchangesError):
    pass


class InvalidRequestError(IntegrationWithExchangesError):
    pass
