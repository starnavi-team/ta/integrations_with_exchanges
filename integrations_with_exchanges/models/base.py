from __future__ import annotations

import abc
import inspect
import threading
import typing
from contextlib import contextmanager
from contextvars import ContextVar
from copy import copy
from inspect import isfunction
from traceback import extract_stack
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from typing import Set
from typing import Type
from typing import TypeVar
from typing import Union

import marshmallow_dataclass
from blinker import signal
from more_itertools import always_iterable

from integrations_with_exchanges.base import DEFAULT_STATE_ATTR
from integrations_with_exchanges.misk.schema_class import Schema as SchemaClass
from integrations_with_exchanges.models.state import InstanceState
from integrations_with_exchanges.utils import get_state
from integrations_with_exchanges.utils import tmp_remove

if typing.TYPE_CHECKING:
    from integrations_with_exchanges.session import Session
    from integrations_with_exchanges.misk.schema_class import Schema


    class HasSchema(typing.Protocol):
        Schema: Type[Schema]


    A = TypeVar('A', bound=HasSchema)

JsonData = Union[str, bytes, bytearray]

IS_SAFE_SPACE_VALUE = ContextVar('IS_SAFE_SPACE_VALUE', default=False)


@contextmanager
def safe_space(is_on=True):
    token = IS_SAFE_SPACE_VALUE.set(is_on)
    try:
        yield
    finally:
        IS_SAFE_SPACE_VALUE.reset(token)


def isinstance_SmartOrder(obj):
    from integrations_with_exchanges.models.orders import SmartOrder
    return isinstance(obj, SmartOrder)


def isinstance_OrderPoint(obj):
    from integrations_with_exchanges.models.orders import OrderPoint
    return isinstance(obj, OrderPoint)


def set_order_to_order_points(order, key, value):
    from integrations_with_exchanges.models.orders import OrderPointGroupType

    order_point_group: Optional[OrderPointGroupType] = None
    if key == 'enter_orders':
        order_point_group = OrderPointGroupType.enter
    if key == 'stop_loss_orders':
        order_point_group = OrderPointGroupType.stop_loss
    if key == 'take_profit_orders':
        order_point_group = OrderPointGroupType.take_profit

    if isinstance(value, list):
        for el in value:
            if isinstance_OrderPoint(el):
                with safe_space(order):
                    el.group_type = order_point_group
                    el.order = order


class Model(abc.ABC):
    class signal:
        update_attr = signal('model_attr_update')

    def __new__(cls, *args, **kwargs):
        obj = super().__new__(cls)
        if not hasattr(obj, DEFAULT_STATE_ATTR):
            object.__setattr__(obj, DEFAULT_STATE_ATTR, InstanceState(obj))

        return obj

    def __hash__(self):
        return hash(get_state(self))

    @classmethod
    def from_dict(cls: Type[A], data: dict, **kwargs) -> A:
        Schema = marshmallow_dataclass.class_schema(cls, base_schema=SchemaClass)

        return Schema(**kwargs).load(data)

    @classmethod
    def from_json(cls: Type[A], data: JsonData, **kwargs) -> A:
        Schema = marshmallow_dataclass.class_schema(cls, base_schema=SchemaClass)

        return Schema(**kwargs).loads(data)

    @safe_space(True)
    def to_dict(self, **kwargs) -> dict:
        Schema = marshmallow_dataclass.class_schema(type(self), base_schema=SchemaClass)
        return Schema(**kwargs).dump(self)

    @safe_space(True)
    def to_json(self, **kwargs) -> str:
        Schema = marshmallow_dataclass.class_schema(type(self), base_schema=SchemaClass)
        return Schema(**kwargs).dumps(self)

    def update(self: A, data: Union[JsonData, dict], **kwargs) -> A:
        Schema = marshmallow_dataclass.class_schema(type(self), base_schema=SchemaClass)
        schema = Schema(partial=True, **kwargs)

        with tmp_remove(schema._hooks[('post_load', False)], 'make_data_class'):
            method_name = 'load' if isinstance(data, dict) else 'loads'
            for k, v in getattr(schema, method_name)(data).items():
                setattr(self, k, v)
        return self

    @safe_space(True)
    def safe_setattr(self, field_name, value):
        setattr(self, field_name, value)

    @property
    def changes(self) -> Dict[str, Any]:
        "Returns changes made on an order_point"
        state = get_state(self)

        res = copy(state.changes)
        for k, obj in vars(self).items():
            if k.startswith('_') or k == 'order': continue

            if isinstance(obj, list):
                for i, o in enumerate(filter(lambda o: isinstance(o, Model), obj)):
                    if changes := o.changes:
                        if k not in res:
                            res[k] = []

                        if res[k] is state.changes.get(k):
                            res[k] = copy(state.changes[k])

                        res[k].append({
                            'op': 'update',
                            'what': o,
                            'fields': changes,
                        })

            elif isinstance(obj, Model):
                if obj.changes:
                    res[k] = obj.changes

        return dict(res)

    @safe_space(True)
    def apply_changes(self):
        state = get_state(self)

        for k, obj in vars(self).items():
            if k.startswith('_') or k == 'order': continue

            if isinstance(obj, list):
                for o in filter(lambda o: isinstance(o, Model), obj):
                    o.apply_changes()
            elif isinstance(obj, Model):
                obj.apply_changes()

        for key, change in state.changes.items():
            if isinstance(change, list):
                continue
            super().__setattr__(key, change['new'])

        state.changes = {}

    @safe_space(True)
    def forget_all_changes(self):
        state = get_state(self)

        for k, obj in vars(self).items():
            if k.startswith('_') or k == 'order': continue

            if isinstance(obj, list):
                for o in filter(lambda o: isinstance(o, Model), obj):
                    o.forget_all_changes()
            elif isinstance(obj, Model):
                obj.forget_all_changes()

        state.changes = {}

    def __setattr__(self, key: str, value):
        if (prop := getattr(type(self), key, None)) is not None:
            if inspect.ismethoddescriptor(prop):
                return object.__setattr__(self, key, value)

        if key.startswith('_') or key == 'order':
            return object.__setattr__(self, key, value)

        state = get_state(self)
        changes = state.changes

        is_safe_space = bool(state.is_safe_space or extract_stack()[-2].name in ('__new__', '__init__'))

        if key in changes:
            changes[key]['new'] = value
        else:
            try:
                old = getattr(self, key)
            except AttributeError:
                if is_safe_space:
                    object.__setattr__(self, key, value)

                    if isinstance(value, list):
                        set_order_to_order_points(self, key, value)
                else:
                    raise RuntimeError('Variable should be initiate only in safe space')
            else:
                if value == old: return  # No need update status

                # If value set from safe space or constructor then not needed save it in history
                if is_safe_space:
                    object.__setattr__(self, key, value)

                    if isinstance(value, ListChangeLogger):
                        set_order_to_order_points(self, key, value)

                else:
                    changes[key] = {
                        'op': 'update',
                        'new': value,
                        'old': old,
                    }

                with safe_space(False):
                    self.signal.update_attr.send(
                        self,
                        attr_name=key,
                        new_value=value,
                        old_value=old,
                        is_safe_space=is_safe_space
                    )

    def __getattribute__(self, key: str):
        res_value = super().__getattribute__(key)

        if key.startswith('_') or key == 'order' or \
            isfunction(res_value):
            return res_value

        state = get_state(self)

        if key in (changes := state.changes):
            change = changes[key]
            if isinstance(change, dict):
                return change['new']
            elif isinstance(change, list):
                if state.is_safe_space:
                    return res_value
                else:
                    return ListChangeLogger(self, key, res_value)
            else:
                raise TypeError(f"Change doesn't have {type(change)} type.")
        else:
            if isinstance(res_value, list):
                if state.is_safe_space:
                    return res_value
                else:
                    return ListChangeLogger(self, key, res_value)
            else:
                return res_value

    @property
    def session(self) -> 'Session':
        state = get_state(self)
        if state.session is None:
            raise ValueError("Not bind with session")
        return state.session

    @property
    def expired_attributes(self) -> Set[str]:
        state = get_state(self)
        return state.expired_attributes

    def expire(self, field_names: Union[str, List[str]]):
        state = get_state(self)
        state.expired_attributes.update(always_iterable(field_names))

        if hasattr(self, 'order'):
            if self.order is None:
                raise ValueError("Order point should be add to order")
            session = get_state(self.order).session
        else:
            session = state.session

        if session:
            session._register_instance_as_expired(self)

    @property
    def is_expired(self) -> bool:
        if isinstance_SmartOrder(self):
            for op in self.points:
                if op.is_expired:
                    return True

        return bool(self.expired_attributes)

    def forget_one_expire(self, attr_name):
        get_state(self).expired_attributes.discard(attr_name)

    def forget_all_expires(self):
        get_state(self).expired_attributes = set()


class ListChangeLogger(list):
    def __init__(self, parent, field_name, target):
        super().__init__()

        self.__parent = parent
        self.__field_name = field_name
        self.__target = target

        from integrations_with_exchanges.models.orders import OrderPointGroupType
        order_point_group: Optional[OrderPointGroupType] = None
        if field_name == 'enter_orders':
            order_point_group = OrderPointGroupType.enter
        if field_name == 'stop_loss_orders':
            order_point_group = OrderPointGroupType.stop_loss
        if field_name == 'take_profit_orders':
            order_point_group = OrderPointGroupType.take_profit

        self.__order_point_group = order_point_group

        self.__lock = threading.RLock()

    def __iter__(self):
        return iter(self.__target)

    def __len__(self):
        with self.__lock:
            return len(self.__target)

    def __getitem__(self, key):
        with self.__lock:
            return self.__target[key]

    def index(self, *args, **kwargs) -> int:
        with self.__lock:
            return self.__target.index(*args, **kwargs)

    def append(self, order_point) -> None:
        if order_point.order is not None:
            raise ValueError("Only not assigned order objects can be added")

        with self.__lock:
            state = get_state(self.__parent)
            changes = state.changes

            if self.__field_name not in changes:
                changes[self.__field_name] = []

            changes[self.__field_name].append({
                'op': 'add',
                'what': order_point
            })

            with safe_space(self.__parent):
                order_point.order = self.__parent
                order_point.group_type = self.__order_point_group

            self.__target.append(order_point)

    # We can't delete anything from collections
    def remove(self, object):
        raise NotImplementedError

    def __delete__(self, instance):
        raise NotImplementedError

    def __delitem__(self, key):
        raise NotImplementedError

    def __delslice__(self, i, j):
        raise NotImplementedError

    def __repr__(self):
        return repr(self.__target)
