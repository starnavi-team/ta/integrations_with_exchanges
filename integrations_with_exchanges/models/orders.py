import typing
import uuid
from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
from decimal import Decimal
from enum import Enum
from itertools import chain
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from uuid import UUID

from more_itertools import first

from integrations_with_exchanges.models.base import Model
from integrations_with_exchanges.utils import is_all_have_status

if typing.TYPE_CHECKING:
    from integrations_with_exchanges.session import Session


@dataclass
class TradingAlgorithms(Model):
    trailing_take_profit: Optional[Dict[str, Any]] = None
    trailing_stop_loss: Optional[Dict[str, Any]] = None
    adjust_stop_loss: Optional[Dict[str, Any]] = None


class OrderType(str, Enum):
    market = 'market'
    limit = 'limit'


class OrderPointGroupType(str, Enum):
    enter = 'enter'
    stop_loss = 'stop_loss'
    take_profit = 'take_profit'


class OrderSide(str, Enum):
    buy = 'buy'
    sell = 'sell'

    def __neg__(self):
        if self == OrderSide.buy:
            return OrderSide.sell
        else:
            return OrderSide.buy


class OrderStatus(str, Enum):
    pending = 'pending'
    open = 'open'
    closed = 'closed'
    canceled = 'canceled'
    rejected = 'rejected'
    expired = 'expired'
    failed = 'failed'

    @property
    def is_not_placed(self):
        return self in {self.canceled, self.rejected, self.expired, self.failed}

    @property
    def is_failed(self):
        return self in {self.rejected, self.expired, self.failed}


@dataclass
class OrderPointModel(Model):
    amount: Decimal = field(metadata={'as_string': True})
    price: Decimal = field(metadata={'as_string': True})
    type: OrderType = field(default=OrderType.limit)
    id: UUID = field(default_factory=uuid.uuid4)
    status: OrderStatus = OrderStatus.pending
    is_condition: bool = False
    is_post_only: bool = False
    additional_info: Dict[str, Any] = field(default_factory=dict)

    class Meta:
        ordered = True


class OrderPoint(OrderPointModel):
    order: Optional['SmartOrder'] = None
    group_type: Optional[OrderPointGroupType] = None

    @property
    def session(self) -> Optional['Session']:
        if order := self.order:
            return order.session
        else:
            return None

    @property
    def side(self) -> Optional[OrderSide]:
        if order := self.order:
            return order.side if self.group_type == OrderPointGroupType.enter else -order.side
        else:
            return None

    @property
    def symbol(self) -> Optional[str]:
        if order := self.order:
            return order.symbol
        else:
            return None

    @property
    def exchange_name(self) -> Optional[str]:
        if order := self.order:
            return order.exchange_name
        else:
            return None

    @property
    def user_credentials(self) -> Optional[Dict[str, str]]:
        if order := self.order:
            return order.user_credentials
        else:
            return None

    @property
    def is_virtual(self) -> Optional[bool]:
        if order := self.order:
            return order.is_virtual
        else:
            return None

    @property
    def is_placed_on_exchange(self) -> bool:
        return 'id' in self.additional_info


@dataclass
class SmartOrderModel(Model):
    symbol: str
    side: OrderSide
    exchange_name: str
    enter_orders: List[OrderPoint]
    stop_loss_orders: List[OrderPoint] = field(default_factory=list)
    take_profit_orders: List[OrderPoint] = field(default_factory=list)
    id: UUID = field(default_factory=uuid.uuid4)
    user_credentials: Optional[Dict[str, str]] = None
    additional_info: Dict[str, Any] = field(default_factory=dict)
    status: OrderStatus = OrderStatus.pending
    is_test: bool = False
    is_virtual: bool = False
    place_now: bool = False
    trading_algorithms: TradingAlgorithms = field(default_factory=TradingAlgorithms)
    created_at: datetime = field(default_factory=datetime.now)
    closed_at: Optional[datetime] = None

    class Meta:
        ordered = True


class SmartOrder(SmartOrderModel):
    @property
    def points(self):
        """Return iterator by all points in order"""

        return chain(self.enter_orders, self.stop_loss_orders, self.take_profit_orders)

    @property
    def is_all_order_points_opened(self) -> bool:
        for o in self.points:
            if o.status != OrderStatus.open:
                return False
        else:
            return True

    @property
    def is_enter_order_closed(self):
        return self.enter_orders[0].status == OrderStatus.closed

    @property
    def is_all_stop_loss_orders_closed(self) -> bool:
        return is_all_have_status(self.stop_loss_orders, OrderStatus.closed)

    @property
    def is_all_take_profit_orders_closed(self) -> bool:
        return is_all_have_status(self.take_profit_orders, OrderStatus.closed)

    @property
    def enter_order(self):
        return self.enter_orders[0]

    @property
    def first_open_stop_loss(self) -> Optional[OrderPoint]:
        return first(filter(lambda o: o.status == OrderStatus.open, self.stop_loss_orders), None)

    @property
    def first_open_take_profit(self) -> Optional[OrderPoint]:
        return first(filter(lambda o: o.status == OrderStatus.open, self.take_profit_orders), None)

    def __post_init__(self):
        self.enter_orders.sort(key=lambda op: op.price, reverse=self.side != OrderSide.buy)
        self.stop_loss_orders.sort(key=lambda op: op.price, reverse=self.side == OrderSide.buy)
        self.take_profit_orders.sort(key=lambda op: op.price, reverse=self.side != OrderSide.buy)
