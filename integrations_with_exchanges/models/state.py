import typing
import weakref
from collections import defaultdict
from typing import Any
from typing import Dict
from typing import Optional
from typing import Set

if typing.TYPE_CHECKING:
    from integrations_with_exchanges import Session


class DefaultDict(defaultdict):
    pass


class InstanceState:
    def __init__(self, instance):
        self.key: Optional[int] = None

        self.changes: Dict[str, Any] = DefaultDict(list)
        self.session: Optional['Session'] = None
        self.expired_attributes: Set[str] = set()
        self.instance = weakref.ref(instance)

    def __hash__(self):
        return hash(self.key)

    @property
    def is_safe_space(self) -> bool:
        from integrations_with_exchanges.models.base import IS_SAFE_SPACE_VALUE
        return IS_SAFE_SPACE_VALUE.get()
