from enum import Enum


class MarginType(str, Enum):
    cross = 'cross'
    isolated = 'isolated'
