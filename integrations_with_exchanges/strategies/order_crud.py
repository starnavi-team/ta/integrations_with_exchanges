from decimal import Decimal
from functools import wraps
from itertools import chain

import ccxt

from integrations_with_exchanges import BaseRestClient
from integrations_with_exchanges import CoinbaseProRestClient
from integrations_with_exchanges.errors import ErrorDescription
from integrations_with_exchanges.models.base import safe_space
from integrations_with_exchanges.models.orders import OrderPoint
from integrations_with_exchanges.models.orders import OrderStatus
from integrations_with_exchanges.models.orders import OrderType
from integrations_with_exchanges.models.orders import SmartOrder
from integrations_with_exchanges.strategies.base import BaseOrderManipulationStrategy
from integrations_with_exchanges.utils import exchange_rest_client


def with_client(f):
    @wraps(f)
    async def decorated(self, order: SmartOrder, *args, **kwargs):
        async with exchange_rest_client(order, self.rest_clients) as client:
            return await f(self, order, *args, client=client, **kwargs)

    return decorated


class _OrderNotPlaced(Exception):
    pass


class OrderCrudStrategy(BaseOrderManipulationStrategy):
    def __init__(self, rest_clients):
        self.rest_clients = rest_clients

    def is_support_object(self, order: SmartOrder):
        return isinstance(order, SmartOrder)

    async def on_complete_sync(self, order: SmartOrder):
        for op in chain((order,), order.points):
            op.forget_all_expires()

    async def on_complete_handle(self, order: SmartOrder):
        order.apply_changes()

    async def set_attr_hoock(self, attr_name, order_info, order_point, client):
        if attr_name == 'price' and order_info['price'] is None:
            if isinstance(client, CoinbaseProRestClient):
                order_info['price'] = Decimal(order_info['cost']) / Decimal(order_info['amount'])
            else:
                if order_point.type == OrderType.market:
                    order_info['price'] = order_point.price
                else:
                    raise ValueError("Price is None")

    @safe_space(True)
    async def synchronize(self, order: SmartOrder) -> None:
        if not order.is_virtual:
            await self._synchronize_non_virtual_orders(order)
        else:
            enter_point = order.enter_order
            if enter_point.status == OrderStatus.pending:
                enter_point.status = OrderStatus.open

    @with_client
    async def _synchronize_non_virtual_orders(self, order: SmartOrder, client: BaseRestClient) -> None:
        # Get information about order points witch has at least one expired status
        # and store it to map [order_id -> info]

        for point in order.points:
            if point.status == OrderStatus.open and point.is_placed_on_exchange:
                expired_attributes = order.expired_attributes | point.expired_attributes
                if expired_attributes:
                    order_info = await client.get_order_info(point)

                    for attr_name in expired_attributes:
                        await self.set_attr_hoock(attr_name, order_info, point, client)

                        attr_type = type(getattr(point, attr_name))
                        setattr(point, attr_name, attr_type(order_info[attr_name]))  # type: ignore

        any_failed = lambda os: any(map(lambda o: o.status == OrderStatus.failed, os))
        any_rejected = lambda os: any(map(lambda o: o.status == OrderStatus.rejected, os))
        all_closed = lambda os: all(list(map(lambda o: o.status == OrderStatus.closed, os)) or [False])

        if any_failed(order.points):
            await self.cancel_opened_order_point(order)
            order.additional_info['reason'] = 'One of order points is failed'
            order.status = OrderStatus.failed
        elif any_rejected(order.points):
            await self.cancel_opened_order_point(order)
            order.additional_info['reason'] = 'One of order points is rejected'
            order.status = OrderStatus.rejected
        elif order.enter_orders[0].status == OrderStatus.canceled:
            await self.cancel_order(order)
            order.additional_info['reason'] = 'Enter order is canceled'
        if order.enter_orders[0].status == OrderStatus.closed and \
            any(map(all_closed, [order.stop_loss_orders, order.take_profit_orders])):
            await self.close_order(order)

    async def handle_new_object(self, order: SmartOrder):
        return await self.try_handle_changes(order)

    async def handle_changed_object(self, order: SmartOrder):
        return await self.try_handle_changes(order)

    @safe_space(True)
    async def try_handle_changes(self, order: SmartOrder) -> None:
        if order.user_credentials is None: return

        try:
            for field_name, change in order.changes.items():
                if isinstance(change,
                              list):  # This will only work for enter_orders, stop_loss_orders, take_profit_orders
                    for change in change:
                        changed_order_point: OrderPoint = change['what']

                        if change['op'] == 'update':
                            # On this point can be two case
                            # First we want update order point parameter
                            # Second we want to close order point
                            if change_status := change['fields'].get('status'):
                                # Cancel order point
                                if (change_status['old'], change_status['new']) == \
                                    (OrderStatus.pending, OrderStatus.open):
                                    await self.open_order_point(order, changed_order_point)
                                elif (change_status['old'], change_status['new']) == \
                                    (OrderStatus.open, OrderStatus.canceled):
                                    await self.cancel_order_point(order, changed_order_point)

                            else:
                                if changed_order_point.status == OrderStatus.open:
                                    await self.update_order_point(order, changed_order_point)

                        if changed_order_point.status.is_failed:
                            await self.handle_bad_order(
                                order, changed_order_point.status, changed_order_point.additional_info['reason'])
                            return
                            # Only for cancel all order
                if field_name == 'status' and change:
                    if (change['old'], change['new']) == (OrderStatus.pending, OrderStatus.open):
                        await self.open_order(order)

                    if (change['old'], change['new']) == (OrderStatus.open, OrderStatus.canceled):
                        await self.cancel_order(order)

                    if (change['old'], change['new']) == (OrderStatus.open, OrderStatus.closed):
                        await self.close_order(order)
        except _OrderNotPlaced:
            await self.cancel_opened_order_point(order)

    @with_client
    async def open_order(self, order: SmartOrder, client: BaseRestClient) -> SmartOrder:
        await client.open_order(order)

        return order

    @with_client
    async def cancel_order(self, order: SmartOrder, client: BaseRestClient) -> SmartOrder:
        await client.cancel_order(order)

        return order

    @with_client
    async def close_order(self, order: SmartOrder, client: BaseRestClient) -> SmartOrder:
        await client.close_order(order)

        return order

    @with_client
    async def open_order_point(
        self,
        order: SmartOrder,
        order_point: OrderPoint,
        client: BaseRestClient
    ) -> OrderPoint:
        try:
            await client.open_order_point(
                order_point
            )
        except ccxt.BaseError as e:
            order_point.status = OrderStatus.rejected
            order_point.additional_info['reason'] = {
                'type': type(e).__name__,
                'exchange': order_point.exchange_name,
                'message': str(e)
            }

            raise _OrderNotPlaced

        return order_point

    @with_client
    async def update_order_point(
        self,
        order: SmartOrder,
        order_point: OrderPoint,
        client: BaseRestClient
    ) -> OrderPoint:
        await client.update_order_point(order_point)

        return order_point

    @with_client
    async def cancel_order_point(
        self,
        order: SmartOrder,
        order_point: OrderPoint,
        client: BaseRestClient
    ) -> OrderPoint:
        await client.cancel_order_point(order_point)

        return order_point

    @with_client
    async def cancel_opened_order_point(
        self,
        order: SmartOrder,
        client: BaseRestClient
    ) -> SmartOrder:
        await client.cancel_opened_order_points(order)

        return order

    @with_client
    async def handle_bad_order(
        self,
        order: SmartOrder,
        status: OrderStatus,
        reason: ErrorDescription,
        client: BaseRestClient
    ) -> SmartOrder:
        await client.cancel_opened_order_points(order)

        order.status = status
        order.additional_info['reason'] = reason

        return order
