from abc import ABC
from itertools import chain

from integrations_with_exchanges.models.orders import SmartOrder
from integrations_with_exchanges.session import SessionStrategy


class BaseOrderManipulationStrategy(SessionStrategy[SmartOrder], ABC):
    async def on_complete_sync(self, order: SmartOrder):
        for op in chain((order,), order.points):
            op.forget_all_expires()

    async def on_complete_handle(self, order: SmartOrder):
        order.apply_changes()

    async def get(self, key) -> SmartOrder:
        return NotImplemented

    async def synchronize(self, order: SmartOrder) -> None:
        return NotImplemented

    async def handle_removed_object(self, order: SmartOrder):
        return NotImplemented
