import operator

from integrations_with_exchanges.base import DEFAULT_STATE_ATTR

instance_state = operator.attrgetter(DEFAULT_STATE_ATTR)

instance_dict = operator.attrgetter("__dict__")
