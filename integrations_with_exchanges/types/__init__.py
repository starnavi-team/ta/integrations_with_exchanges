from decimal import Decimal
from typing import Union

Number = Union[Decimal, int, float, complex]
MarginLevel = Number

