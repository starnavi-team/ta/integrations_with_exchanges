from decimal import Decimal

import pytest

from integrations_with_exchanges import BaseRestClient
from integrations_with_exchanges.models.orders import OrderSide


@pytest.fixture
async def price(client: BaseRestClient, side: OrderSide, symbol: str, tick_size):
    order_book = await client.get_order_book(symbol)
    price = order_book[
        'bids' if side == OrderSide.buy else 'asks'
    ][10][0]

    if side == OrderSide.buy:
        price *= 0.8
    else:
        price *= 1.2

    price = price // tick_size * tick_size

    return Decimal(price)
