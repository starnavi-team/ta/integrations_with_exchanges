from decimal import Decimal

import pytest

from integrations_with_exchanges.clients.rest import BaseRestClient
from integrations_with_exchanges.models.orders import OrderPoint
from integrations_with_exchanges.models.orders import OrderSide
from integrations_with_exchanges.models.orders import OrderStatus
from integrations_with_exchanges.models.orders import OrderType
from integrations_with_exchanges.models.orders import SmartOrder
from integrations_with_exchanges.session import Session
from tests.conftest import exchanges


@pytest.mark.parametrize('exchange_name, exchange_class, user_credentials, is_test', exchanges())
@pytest.mark.parametrize('symbol', ['BTC/USD'])
@pytest.mark.asyncio
async def test_get_order_book(
    exchange_name: str,
    exchange_class: BaseRestClient,
    user_credentials: dict,
    symbol: str,
    is_test

):
    client = await exchange_class.create(**user_credentials, is_test=is_test)
    res = await client.get_order_book(symbol)
    assert 'bids' in res and 'asks' in res


@pytest.mark.parametrize('exchange_name, exchange_class, user_credentials, is_test',
                         list(filter(lambda o: o[0] == 'binancefuture', exchanges())))
@pytest.mark.parametrize('symbol', ['BTC/USDT'])
@pytest.mark.parametrize('price', [Decimal('10000')])
@pytest.mark.parametrize('order_type', [
    OrderType.market,
    OrderType.limit
])
@pytest.mark.parametrize('side', [OrderSide.buy,
                                  # OrderSide.sell
                                  ])
@pytest.mark.asyncio
async def test_crud(
    session: Session,
    exchange_class,
    order_type: OrderType,
    order: SmartOrder,
):
    order.enter_order.type = order_type

    order.status = OrderStatus.open  # Открываем

    for op in order.points:
        if op.status == OrderStatus.pending:
            op.status = OrderStatus.open

    order.take_profit_orders[0].type = OrderType.market
    order.take_profit_orders[0].status = OrderStatus.open

    session.add(order)

    # order.enter_orders[0].amount = 8
    # order.stop_loss_orders[0].amount = 8
    # order.take_profit_orders[0].amount = 8

    await session.commit()

    assert order.status == OrderStatus.open, order.additional_info
    assert all(map(lambda o: o.status == OrderStatus.open, order.points))

    assert order.changes == {}

    order.stop_loss_orders[0].status = OrderStatus.canceled

    await session.commit()
    assert order.changes == {}

    order.stop_loss_orders.append(OrderPoint(amount=Decimal("10"), price=Decimal("5000")))

    await session.commit()
    assert order.changes == {}

    order.enter_orders[0].expire(['status'])
    assert order.enter_orders[0].expired_attributes == {'status'}

    await session.synchronize()
    assert order.enter_orders[0].expired_attributes == set()
    assert order.changes == {}

    order.status = OrderStatus.closed

    await session.commit()
    assert order.changes == {}

    for op in order.points:
        assert op.status != OrderStatus.open, f"Order point with id: {op.id} have open status"
