import asyncio

import pytest
from more_itertools import first

from integrations_with_exchanges import BinanceUsWebsocketClient
from integrations_with_exchanges import BinanceWebsocketClient
from integrations_with_exchanges import BitmexWebsocketClient
from integrations_with_exchanges import BinanceFutureWebsocketClient
from integrations_with_exchanges import CoinbaseProWebsocketClient


@pytest.mark.asyncio
@pytest.mark.parametrize('websocket_client_class, symbol', [
    #(BitmexWebsocketClient, 'BTC/USD'),
    #(BinanceWebsocketClient, 'BTC/USDT'),
    (BinanceFutureWebsocketClient, 'BTC/USDT'),
    #(BinanceUsWebsocketClient, 'BTC/USDT'),
    #(CoinbaseProWebsocketClient, 'BTC/USD')
])
@pytest.mark.parametrize('is_test', [True, False])
async def test_read_prices(websocket_client_class, symbol, is_test):
    call_count = 0

    class MyWebsocket(websocket_client_class):
        async def on_message(self, message):
            nonlocal call_count
            call_count += 1
            if call_count > 2:
                self.stop()

        async def on_error(self, message):
            raise Exception(message)

    websocket = MyWebsocket(is_test=is_test)
    websocket.subscribe_on(topic='trade', symbol=symbol)

    done, pending = await asyncio.wait([
        websocket.run()
    ], timeout=60)

    assert done and await first(done) is None and not pending
