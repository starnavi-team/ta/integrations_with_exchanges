import pytest

from tests.conftest import exchanges


@pytest.mark.parametrize('exchange_name, exchange_class, user_credentials, is_test',
                         list(filter(lambda o: o[0] == 'bitmex', exchanges())))
@pytest.mark.asyncio
async def test_get_margin(exchange_name, client):
    await client.get_margin('btc/usd')


@pytest.mark.parametrize('exchange_name, exchange_class, user_credentials, is_test',
                         list(filter(lambda o: o[0] == 'bitmex', exchanges())))
@pytest.mark.asyncio
async def test_set_margin(exchange_name, client):
    await client.set_margin('btc/usd', 0)
