from decimal import Decimal
from unittest.mock import AsyncMock
from unittest.mock import MagicMock

import pytest

from integrations_with_exchanges import BaseRestClient
from integrations_with_exchanges import Session
from integrations_with_exchanges.models.orders import OrderSide
from integrations_with_exchanges.models.orders import OrderStatus
from integrations_with_exchanges.strategies.order_crud import OrderCrudStrategy
from integrations_with_exchanges.utils import get_state
from tests.conftest import exchanges


@pytest.fixture
def rest_client():
    client = MagicMock(BaseRestClient)
    yield client
    client.close.assert_awaited()


@pytest.fixture
def session(rest_client):
    return Session(
        OrderCrudStrategy({
            'bitmex': MagicMock(
                create=AsyncMock(
                    return_value=rest_client
                )
            )}
        )
    )


@pytest.fixture
def session_with_order(rest_client, order):
    sess = Session(
        OrderCrudStrategy({
            'bitmex': MagicMock(
                create=AsyncMock(
                    return_value=rest_client
                )
            )}
        )
    )

    sess._objects[get_state(order)] = order
    return sess


@pytest.mark.parametrize(
    'exchange_name, _, user_credentials, is_test',
    list(filter(lambda o: o[0] == 'bitmex', exchanges()))
)
@pytest.mark.parametrize('symbol', ['BTC/USD'])
@pytest.mark.parametrize('side', [OrderSide.buy])
@pytest.mark.parametrize('price', [Decimal("10000")])
class TestCRUD:
    # Tests Create

    @pytest.mark.asyncio
    async def test_open_order_when_set_open_before_add(self, session, order, rest_client, _):
        order.status = OrderStatus.open
        session.add(order)

        await session.commit()

        assert order.status == OrderStatus.open
        assert rest_client.open_order.called

    @pytest.mark.asyncio
    async def test_open_order_when_set_open_after_add(self, session, order, rest_client, _):
        session.add(order)

        order.status = OrderStatus.open

        await session.commit()

        assert order.status == OrderStatus.open
        assert rest_client.open_order.called

    # Tests Cancel

    @pytest.mark.asyncio
    @pytest.mark.parametrize('status', [OrderStatus.open])
    async def test_cancel_order_when_set_canceled_before_add(
            self, session, order, rest_client, status_before_test, _
    ):
        order.status = OrderStatus.canceled
        session.add(order)

        await session.commit()

        assert order.status == OrderStatus.canceled
        assert rest_client.cancel_order.called

    @pytest.mark.asyncio
    @pytest.mark.parametrize('status', [OrderStatus.open])
    async def test_cancel_order_when_set_canceled_after_add(
            self, session, order, rest_client, status_before_test, _
    ):
        session.add(order)

        order.status = OrderStatus.canceled

        await session.commit()

        assert order.status == OrderStatus.canceled
        assert rest_client.cancel_order.called

    @pytest.mark.asyncio
    async def test_call_create_order_point_when_set_open_before_add(
            self, session_with_order, order, rest_client, order_point, _
    ):
        order_point.status = OrderStatus.open

        order.take_profit_orders.append(order_point)

        await session_with_order.commit()

        rest_client.open_order_point.assert_called()
        rest_client.update_order_point.assert_not_called()
        rest_client.cancel_order_point.assert_not_called()

    @pytest.mark.asyncio
    async def test_call_create_order_point_when_set_open_after_add(
            self, session_with_order, order, rest_client, order_point, _
    ):
        order.take_profit_orders.append(order_point)

        order_point.status = OrderStatus.open

        await session_with_order.commit()

        rest_client.open_order_point.assert_called()
        rest_client.update_order_point.assert_not_called()
        rest_client.cancel_order_point.assert_not_called()
