from integrations_with_exchanges.errors.__init__ import format_ccxt_errors
import pytest
from ccxt.base.errors import AuthenticationError, PermissionDenied, ExchangeError, \
    InsufficientFunds, DDoSProtection, AccountSuspended, ExchangeNotAvailable


class TestBaseErrorsRemap:

    def test_wrong_excaption_type(self):
        """
        raise division by zero (or any other NOT! BaseError type) in result: raising exception  ValueError
        """
        with pytest.raises(TypeError):
            assert format_ccxt_errors(ZeroDivisionError('Zero division'))

    @pytest.mark.parametrize('error, expect', [
        (
                AuthenticationError('{"error": "some string"}'),
                {'type': 'AuthenticationError', 'message': '{"error": "some string"}', 'exchange': None}
        ),
        (
                ExchangeError('bitmex  {"error": "some string"}'),
                {'type': 'ExchangeError', 'exchange': 'bitmex', 'message': 'some string'}
        ),
        (
                PermissionDenied('bitmex {"error":{"message":"some string"}}'),
                {'type': 'PermissionDenied', 'exchange': 'bitmex', 'message': 'some string'}
        ),
        (
                ExchangeNotAvailable('coinbase  {"some_key": "Some value"} '),
                {'type': 'ExchangeNotAvailable', 'exchange': 'coinbase', 'message': '{"some_key": "Some value"}'}
        ),
        (
                ExchangeNotAvailable('coinbase  {"error": "Some value"} '),
                {'type': 'ExchangeNotAvailable', 'exchange': 'coinbase', 'message': 'Some value'}
        ),
        (
                InsufficientFunds('coinbase Insufficient funds'),
                {'type': 'InsufficientFunds', 'exchange': 'coinbase', 'message': 'Insufficient funds'}
        ),
        (
                InsufficientFunds('bitmex {"error": ["val1", "val2", "val3", 4]}'),
                {'type': 'InsufficientFunds', 'exchange': 'bitmex', 'message': '["val1", "val2", "val3", 4]'}
        ),
        (
                DDoSProtection('[{"key_1": "val_1"}, {"key_2": 44}]'),
                {'type': 'DDoSProtection', 'message': '[{"key_1": "val_1"}, {"key_2": 44}]', 'exchange': None}
        ),
        (
                AccountSuspended('coinbase [{"key_1": "val_1"}, {"key_2": 44}]'),
                {'type': 'AccountSuspended', 'exchange': 'coinbase',
                 'message': '[{"key_1": "val_1"}, {"key_2": 44}]'}
        )

    ])
    def test_errors_formatting(self, error, expect):

        try:
            raise error
        except Exception as e:
            result = format_ccxt_errors(e)
        assert result == expect
