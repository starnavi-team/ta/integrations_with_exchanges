from decimal import Decimal
from operator import add
from operator import sub
from unittest.mock import MagicMock

import pytest

from integrations_with_exchanges import BitmexRestClient
from integrations_with_exchanges import CoinbaseProRestClient
from integrations_with_exchanges import REST_CLIENTS
from integrations_with_exchanges.clients.rest import BaseRestClient
from integrations_with_exchanges.clients.rest import BinanceFutureRestClient
from integrations_with_exchanges.models.base import safe_space
from integrations_with_exchanges.models.orders import OrderPoint
from integrations_with_exchanges.models.orders import OrderSide
from integrations_with_exchanges.models.orders import OrderType
from integrations_with_exchanges.models.orders import SmartOrder
from integrations_with_exchanges.session import Session
from integrations_with_exchanges.session import SessionStrategy
from integrations_with_exchanges.strategies.order_crud import OrderCrudStrategy


def exchanges():
    yield 'bitmex', BitmexRestClient, {
        'api_key': 'BnNeZoOfo2NpZXU9o3YOO0AQ',
        'api_secret': '9i3TSn1fxgbrwojDolxvFD8IYutWBJKhbO90tw9nj0okkHnK'
    }, True
    yield 'binancefuture', BinanceFutureRestClient, {
        'api_key': 'eb09e070f79f2c8542bebe049b10bc9c512154bdbcb4d387a3b56c706207f592',
        'api_secret': '794bdd753f8d0a3dad16b2bd6fa9af8b32836e3167297c4fd3598935b679bcbf'
    }, True
    yield 'coinbasepro', CoinbaseProRestClient, {
        "api_key": "21788cd68c9f19c76cd4b7b478ee898a",
        "api_secret": "g1C1XJwzrcMjXMGDJwIpGbLV96S1hLFyFTm5KAwmAK49lbJh/A6dHus3V/mD1ko/SrWB19gC0tpLWTwbaJFfBQ==",
        "password": "6vekneciufx"
    }, True


def any_one_exchange():
    yield 'bitmex', BitmexRestClient, {
        'api_key': 'BnNeZoOfo2NpZXU9o3YOO0AQ',
        'api_secret': '9i3TSn1fxgbrwojDolxvFD8IYutWBJKhbO90tw9nj0okkHnK'
    }, True


@pytest.fixture
async def client(
    exchange_class: BaseRestClient,
    user_credentials: dict,
    is_test: bool,
):
    cl = await exchange_class.create(**user_credentials, is_test=is_test)

    yield cl

    await cl.close()


@pytest.fixture
async def tick_size(client, symbol):
    await client._client.load_markets()
    return client._client.markets[symbol]['precision']['price']


@pytest.fixture
def price():
    return Decimal("10000")


@pytest.fixture
def order(symbol, side, price, exchange_name, user_credentials, is_test):
    return SmartOrder(
        symbol=symbol,
        side=side,
        exchange_name=exchange_name,  # need only in open_order service
        user_credentials=user_credentials,
        enter_orders=[OrderPoint(amount=Decimal('1'), price=price, type=OrderType.limit)],
        stop_loss_orders=[OrderPoint(amount=Decimal('1'),
                                     price=(sub if side == OrderSide.buy else add)(price, 1000),
                                     type=OrderType.limit)],
        take_profit_orders=[OrderPoint(amount=Decimal('1'),
                                       price=(add if side == OrderSide.buy else sub)(price, 1000),
                                       type=OrderType.limit)],
        is_test=is_test,
    )


@pytest.fixture
def status_before_test(order, status):
    with safe_space(order):
        order.status = status


@pytest.fixture
def dummy_order_strategy(supported_objects):
    magic = MagicMock(SessionStrategy)
    magic.is_support_object.side_effect = lambda o: isinstance(o, supported_objects)
    return magic


@pytest.fixture
def order_strategy():
    return OrderCrudStrategy(REST_CLIENTS)


@pytest.fixture
def session(order_strategy):
    return Session(order_strategy)


@pytest.fixture
def dummy_session(dummy_order_strategy):
    return Session(dummy_order_strategy)


@pytest.fixture
def order_point(order):
    return OrderPoint(amount=Decimal('1'), price=price, type=OrderType.limit)
