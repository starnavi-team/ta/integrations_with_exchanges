import asyncio
from unittest.mock import MagicMock

import pytest
import websockets
from websockets import ConnectionClosedError

from integrations_with_exchanges import WEBSOCKET_CLIENTS


@pytest.mark.parametrize('websocket_class', WEBSOCKET_CLIENTS.values())
@pytest.mark.filterwarnings("")
@pytest.mark.asyncio
async def test_reconect_ater_ConnectionClosedError(websocket_class, monkeypatch):
    websockets.connect = MagicMock()

    class TestWebsocketClient(websocket_class):
        async def on_message(self, message): # pragma: no cover
            pass

    websocket = TestWebsocketClient()

    called = set()

    async def method_which_raise_exception(self, *args, **kwargs):
        if id(self) not in called:
            called.add(id(self))
            raise ConnectionClosedError(1006, "")
        else:
            websocket.stop()

    monkeypatch.setattr(websocket_class, '_handle_commands', method_which_raise_exception)
    monkeypatch.setattr(websocket_class, '_listen_new_messages', method_which_raise_exception)

    await asyncio.wait([
        websocket.run()
    ], timeout=0.1)
