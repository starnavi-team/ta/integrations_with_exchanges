from decimal import Decimal

import pytest

from integrations_with_exchanges.models.base import Model
from integrations_with_exchanges.models.orders import OrderSide
from integrations_with_exchanges.models.orders import SmartOrder
from tests.conftest import any_one_exchange


@pytest.mark.parametrize('exchange_name, exchange_class, user_credentials, is_test', any_one_exchange())
@pytest.mark.parametrize('symbol', ['BTC/USD'])
@pytest.mark.parametrize('side', [OrderSide.buy])
@pytest.mark.parametrize('price', [Decimal("10000")])
@pytest.mark.parametrize('supported_objects', [Model])
@pytest.mark.asyncio
async def test(order: SmartOrder, price, dummy_order_strategy, dummy_session, exchange_class):
    dummy_session.add(order)

    assert order.session is dummy_session
    assert all(map(lambda o: o.session is dummy_session, order.points))
