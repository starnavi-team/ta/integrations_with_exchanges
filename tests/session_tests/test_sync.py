from decimal import Decimal

import pytest

from integrations_with_exchanges.models.orders import OrderSide
from integrations_with_exchanges.models.orders import SmartOrder
from tests.conftest import exchanges


@pytest.fixture
def session_with_order(order, dummy_session):
    dummy_session._objects[order.id] = order
    order._session = dummy_session
    return dummy_session


@pytest.mark.parametrize(
    'exchange_name, _, user_credentials, is_test',
    list(filter(lambda o: o[0] == 'bitmex', exchanges()))
)
@pytest.mark.parametrize('symbol', ['BTC/USD'])
@pytest.mark.parametrize('side', [OrderSide.buy, OrderSide.sell])
@pytest.mark.parametrize('price', [Decimal("10000")])
@pytest.mark.parametrize('supported_objects', [SmartOrder])
@pytest.mark.asyncio
async def test(order: SmartOrder, price, dummy_order_strategy, session_with_order, _):
    order.expire(['status'])

    await session_with_order.synchronize()

    assert order.expired_attributes == {'status'}
    dummy_order_strategy.synchronize.assert_called_with(order)
