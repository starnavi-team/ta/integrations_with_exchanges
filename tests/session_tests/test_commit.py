from decimal import Decimal

import pytest

from integrations_with_exchanges.models.orders import OrderSide
from integrations_with_exchanges.models.orders import SmartOrder
from tests.conftest import any_one_exchange
from tests.conftest import exchanges


@pytest.fixture
def price():
    return Decimal("10000")


@pytest.mark.parametrize('exchange_name, exchange_class, user_credentials, is_test', any_one_exchange())
@pytest.mark.parametrize('symbol', ['BTC/USD'])
@pytest.mark.parametrize('side', [OrderSide.buy, OrderSide.sell])
@pytest.mark.parametrize('supported_objects', [SmartOrder])
@pytest.mark.asyncio
async def test(order: SmartOrder, price, dummy_order_strategy, dummy_session, exchange_class):
    dummy_session.add(order)

    await dummy_session.commit()

    dummy_order_strategy.handle_new_object.assert_called_with(order)
