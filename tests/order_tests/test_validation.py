from decimal import Decimal

import pytest

from integrations_with_exchanges.models.orders import OrderSide
from tests.conftest import exchanges


@pytest.mark.parametrize('exchange_name, exchange_class, user_credentials, is_test', exchanges())
@pytest.mark.parametrize('symbol', ['BTC/USD'])
@pytest.mark.parametrize('side', [OrderSide.buy, OrderSide.sell])
@pytest.mark.parametrize('price', [Decimal("10000")])
def test_exception_when_order_have_not_a_validate_order_of_order_points(order, exchange_class):
    # serialize
    serialized = order.to_dict()

    serialized['take_profit_orders'][-1]['price'] = '1' if order.side == OrderSide.buy else '99999999'
