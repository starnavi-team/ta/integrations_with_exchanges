from decimal import Decimal
from unittest import mock

import pytest
from blinker import signal

from integrations_with_exchanges.models.orders import OrderPoint
from integrations_with_exchanges.models.orders import OrderSide
from integrations_with_exchanges.models.orders import SmartOrder
from tests.conftest import exchanges


@pytest.mark.parametrize('exchange_name, exchange_class, user_credentials, is_test', exchanges())
@pytest.mark.parametrize('symbol', ['BTC/USD'])
@pytest.mark.parametrize('side', [OrderSide.buy, OrderSide.sell])
@pytest.mark.parametrize('price', [Decimal("10000")])
@pytest.mark.asyncio
async def test_change_some_field(order: SmartOrder, price, exchange_class):
    order.symbol = 'ETC/USD'

    assert order.changes == {
        'symbol': {
            'op': 'update',
            'new': 'ETC/USD',
            'old': 'BTC/USD'
        }
    }
    assert order.symbol == 'ETC/USD'

    order.symbol = 'CTC/USD'

    assert order.changes == {
        'symbol': {
            'op': 'update',
            'new': 'CTC/USD',
            'old': 'BTC/USD'
        }
    }
    assert order.symbol == 'CTC/USD'


@pytest.mark.parametrize('exchange_name, exchange_class, user_credentials, is_test', exchanges())
@pytest.mark.parametrize('symbol', ['BTC/USD'])
@pytest.mark.parametrize('side', [OrderSide.buy, OrderSide.sell])
@pytest.mark.parametrize('price', [Decimal("10000")])
@pytest.mark.parametrize('order_point', [OrderPoint(price=Decimal('10'), amount=Decimal('1'))])
@pytest.mark.parametrize('where', ['enter_orders', 'stop_loss_orders', 'take_profit_orders'])
@pytest.mark.asyncio
async def test_add_new_order_point(order: SmartOrder, order_point, where, exchange_class):
    order_point.order = None  # Pytest doesn't do copy of result of fixture
    getattr(order, where).append(order_point)

    assert order.changes == {
        where: [{
            'op': 'add',
            'what': order_point
        }]
    }

    order.apply_changes()

    assert order.changes == {}


@pytest.mark.parametrize('exchange_name, exchange_class, user_credentials, is_test', exchanges())
@pytest.mark.parametrize('symbol', ['BTC/USD'])
@pytest.mark.parametrize('side', [OrderSide.buy, OrderSide.sell])
@pytest.mark.parametrize('price', [Decimal("10000")])
@pytest.mark.parametrize('new_price', [Decimal("10001")])
@pytest.mark.parametrize('where', ['enter_orders', 'stop_loss_orders', 'take_profit_orders'])
@pytest.mark.asyncio
async def test_update_order_point(order: SmartOrder, where, price, new_price, exchange_class):
    old_price = getattr(order, where)[0].price
    getattr(order, where)[0].price = new_price

    assert order.changes == {
        where: [{
            'op': 'update',
            'what': getattr(order, where)[0],
            'fields': {
                'price': {
                    'op': 'update',
                    'new': new_price,
                    'old': old_price,
                }
            }
        }],
    }


@pytest.mark.parametrize('exchange_name, exchange_class, user_credentials, is_test', exchanges())
@pytest.mark.parametrize('symbol', ['BTC/USD'])
@pytest.mark.parametrize('side', [OrderSide.buy, OrderSide.sell])
@pytest.mark.parametrize('price', [Decimal("10000")])
@pytest.mark.asyncio
async def test_clear_change(order: SmartOrder, price, exchange_class):
    order.symbol = 'ETC/USD'

    assert order.changes == {
        'symbol': {
            'op': 'update',
            'new': 'ETC/USD',
            'old': 'BTC/USD'
        }
    }

    order.apply_changes()

    assert order.changes == {}

    assert order.symbol == 'ETC/USD'


@pytest.mark.parametrize('exchange_name, exchange_class, user_credentials, is_test', exchanges())
@pytest.mark.parametrize('symbol', ['BTC/USD'])
@pytest.mark.parametrize('side', [OrderSide.buy, OrderSide.sell])
@pytest.mark.parametrize('method', ['dict', 'json'])
@pytest.mark.parametrize('is_from_user', [True, False])
def test_dump_and_load(order, exchange_class, method, is_from_user):
    # serialize
    serialize_method = getattr(SmartOrder, f'to_{method}')
    serialized = serialize_method(order)

    dummy_func = mock.MagicMock()
    signal('model_attr_update').connect(lambda *args, **kwargs: dummy_func(*args, **kwargs), weak=False)

    # deserialize
    deserialize_method = getattr(SmartOrder, f'from_{method}')
    deserialized_order = deserialize_method(serialized, context={'is_from_user': is_from_user})

    # dummy_func.assert_not_called()

    assert deserialized_order == order
    assert all(map(lambda op: op.order is deserialized_order, deserialized_order.points))


@pytest.mark.parametrize('exchange_name, _, user_credentials, is_test', exchanges())
@pytest.mark.parametrize('symbol', ['BTC/USD'])
@pytest.mark.parametrize('side', [OrderSide.buy, OrderSide.sell])
@pytest.mark.parametrize('price', [Decimal("10000")])
@pytest.mark.parametrize('add_to', ['enter_orders', 'stop_loss_orders', 'take_profit_orders'])
def test_add_order_point_to_order(order, order_point, add_to, _):
    # Before add order point
    assert order_point.order is None
    assert order_point.symbol is None
    assert order_point.side is None
    assert order_point.group_type is None

    getattr(order, add_to).append(order_point)

    assert order_point.order == order
    assert order_point.symbol == order.symbol
    assert order_point.group_type is not None
    assert order_point.side == (order.side if add_to == 'enter_orders' else -order.side)
