import asyncio
from decimal import Decimal

from integrations_with_exchanges import REST_CLIENTS
from integrations_with_exchanges import Session
from integrations_with_exchanges.models.orders import OrderPoint
from integrations_with_exchanges.models.orders import OrderSide
from integrations_with_exchanges.models.orders import OrderStatus
from integrations_with_exchanges.models.orders import OrderType
from integrations_with_exchanges.models.orders import SmartOrder
from integrations_with_exchanges.strategies.order_crud import OrderCrudStrategy


async def main():
    session = Session(OrderCrudStrategy(REST_CLIENTS))

    order = SmartOrder(
        symbol='BTC/USD',
        side=OrderSide.buy,
        exchange_name='bitmex',
        user_credentials={...},
        enter_orders=[OrderPoint(amount=Decimal('1'), price=Decimal("8000"), type=OrderType.limit)],
        stop_loss_orders=[OrderPoint(amount=Decimal('1'), price=Decimal("7000"), type=OrderType.limit)],
        take_profit_orders=[OrderPoint(amount=Decimal('1'), price=Decimal("7000"), type=OrderType.limit)],
    )

    await session.add(order)

    order.status = OrderStatus.open

    await session.commit()


if __name__ == '__main__':
    asyncio.run(main())
